﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autonomo.Class
{
    public class Obtaining
    {
        /// <summary>
        /// Método que permite obtener el primer día del mes...
        /// </summary>
        /// <returns></returns>
        public static DateTime GetFirstDayMonth()
        {
            string month = String.Format("{0:00}", DateTime.Now.Month); //01,02...12.
            string year = DateTime.Now.Year.ToString();
            string date = $"01/{month}/{year}";

            return Convert.ToDateTime(date);
        }


        //
    }
}
