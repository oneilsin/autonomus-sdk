﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autonomo.Class
{
    public partial class Enumerable
    {
        public enum ScreamState { Normal = 0, Maximize = 1 }

        public enum SqlIdentity
        {
            SqlServer=1,
            SqlBase=2,
            SqlUser=3,
            SqlPassword=4
        }
    }
}
