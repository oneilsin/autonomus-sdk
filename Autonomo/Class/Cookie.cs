﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autonomo.Class
{
    public class Cookie
    {
        public Cookie()
        {
            module = string.Empty;
            idModule = string.Empty;
            server = string.Empty;
            dataBase = string.Empty;
            idUser = string.Empty;
            userName = string.Empty;
            version = string.Empty;
            role = new string[] {""};
            igv = 0m;
            exchange = 0m;
            profile = Properties.Resources.Photo;
            password = string.Empty;
            userFullName = string.Empty;
            sqlPassword = string.Empty;
            sqlUser = string.Empty;
        }
        private string module;
        private string idModule;
        private string server;
        private string dataBase;
        private string idUser;
        private string userName;
        private string version;
        private string[] role;
        private decimal igv;
        private decimal exchange;
        private Image profile;
        private string password;
        private string userFullName;
        private string sqlPassword;
        private string sqlUser;

        public string Module { get => module; set => module = value; }
        public string IdModule { get => idModule; set => idModule = value; }
        public string Server { get => server; set => server = value; }
        public string DataBase { get => dataBase; set => dataBase = value; }
        public string IdUser { get => idUser; set => idUser = value; }
        public string UserName { get => userName; set => userName = value; }
        public string Version { get => version; set => version = value; }
        public string[] Role { get => role; set => role = value; }
        public decimal Igv { get => igv; set => igv = value; }
        public decimal Exchange { get => exchange; set => exchange = value; }
        public Image Profile { get => profile; set => profile = value; }
        public string Password { get => password; set => password = value; }
        public string UserFullName { get => userFullName; set => userFullName = value; }
        public string SqlPassword { get => sqlPassword; set => sqlPassword = value; }
        public string SqlUser { get => sqlUser; set => sqlUser = value; }
    }
}
