﻿using Autonomo.CustomControls;
using Autonomo.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autonomo.Class
{
    public class SqlContext
    {
        static SqlModel _sql;

        public SqlContext()
        {

        }

        static string StringServerConnection
        {
            get
            {
                return $"server={_sql.Server}; user id={_sql.User}; password={_sql.Password}";
            }
        }

        static string StringSqlConnection
        {
            get
            {
                return $"Data Source={_sql.Server}; Initial Catalog={_sql.Db}; User ID={_sql.User} ;Password={_sql.Password}";
            }
        }

        public static DataSet ExecuteSqlCommand(string query, string connection)
        {
            StringBuilder sb = new StringBuilder(connection);
            using (SqlConnection sql = new SqlConnection(sb.ToString()))
            {
                try
                {
                    sql.Open();
                    var adapter = new SqlDataAdapter(query, sql);
                    var ds = new DataSet();
                    adapter.Fill(ds);
                    return ds;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public static DataSet ExecuteSqlCommand(string query, SqlModel sqlConnection)
        {
            _sql = new SqlModel()
            {
                Server = sqlConnection.Server,
                Db = sqlConnection.Db,
                User = sqlConnection.User,
                Password = sqlConnection.Password
            };

            StringBuilder sb = new StringBuilder(StringSqlConnection);
            using (SqlConnection sql = new SqlConnection(sb.ToString()))
            {
                try
                {
                    sql.Open();
                    var adapter = new SqlDataAdapter(query, sql);
                    var ds = new DataSet();
                    adapter.Fill(ds);
                    return ds;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public static bool GetSqlConnection(FlatComboBox box, SqlModel sql)
        {
            _sql = new SqlModel()
            {
                Server = sql.Server,
                User = sql.User,
                Password = sql.Password
            };

            string sqlQuery = "SELECT name " +
                "FROM sys.databases " +
                "WHERE LTRIM(RTRIM(name)) NOT LIKE'%temp%' " +
                "AND name NOT IN('master','model','msdb','ReportServer')";
            try
            {
                var ds = ExecuteSqlCommand(sqlQuery, StringServerConnection);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    box.DataSource = ds.Tables[0];
                    box.DisplayMember = "name";
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
