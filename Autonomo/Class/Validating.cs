﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autonomo.Class
{
    public class Validating
    {

        public static void NoSpaces(KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) || char.IsDigit(e.KeyChar))
                e.Handled = false;
            else if (char.IsControl(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = true;
        }


        public static void OnlyText(KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar))
                e.Handled = false;
            else if (char.IsSeparator(e.KeyChar))
                e.Handled = false;
            else if (char.IsControl(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = true;
        }

        public static void OnlyNumber(KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
                e.Handled = false;
            else if (char.IsControl(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = true;
        }

        public static void OnlyDecimal(KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
                e.Handled = false;
            else if (char.IsControl(e.KeyChar))
                e.Handled = false;
            else if (char.IsSeparator(e.KeyChar))
                e.Handled = false;
            else if (e.KeyChar.ToString() == ".")
                e.Handled = false;
            else
                e.Handled = true;
        }
        public static void OnlyDate(KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
                e.Handled = false;
            else if (char.IsControl(e.KeyChar))
                e.Handled = false;
            else if (e.KeyChar.ToString() == "/") //  01/01/2020
                e.Handled = false;
            else if (e.KeyChar.ToString() == "-") // 01-01-2020
                e.Handled = false;
            else
                e.Handled = true;
        }
        public static void OnlyEmail(KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
                e.Handled = false;
            if (char.IsLetter(e.KeyChar))
                e.Handled = false;
            else if (char.IsControl(e.KeyChar))
                e.Handled = false;
            else if (e.KeyChar.ToString() == "-") //  algo-mas@mail.com
                e.Handled = false;
            else if (e.KeyChar.ToString() == "_") // algo_mas@mail.com
                e.Handled = false;
            else if (e.KeyChar.ToString() == "@") // algo@mail.com
                e.Handled = false;
            else if (e.KeyChar.ToString() == ".") // algo@mail.com
                e.Handled = false;
            else
                e.Handled = true;
        }

        public static void OnlyTime(KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
                e.Handled = false;
            else if (char.IsSeparator(e.KeyChar))
                e.Handled = false;
            else if (char.IsControl(e.KeyChar))
                e.Handled = false;
            else if (e.KeyChar.ToString().Equals(":"))
                e.Handled = false;
            else
                e.Handled = true;
        }
        public static void OnlyPhone(KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
                e.Handled = false;
            else if (char.IsControl(e.KeyChar))
                e.Handled = false;
            else if (char.IsSeparator(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = true;
        }

        /// <summary>
        /// Método que permite evaluar si el valor insertado en una Grilla ya ha sido registrada, retorna "True" si ya existe el valor en la grilla.
        /// </summary>
        /// <param name="column">Parámetro--> Referencia al nombre de la columna</param>
        /// <param name="match">Parámetro--> Referencia al valor de la columna</param>
        /// <param name="grid"></param>
        /// <returns></returns>
        public static bool ValidateExistDataOnGrid(string column, string match, DataGridView grid)
        {

            if (grid.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in grid.Rows)
                {
                    if(row.Cells[column].Value.ToString().Equals(match))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Método que permite evaluar si el valor insertado en una Grilla ya ha sido registrada, retorna "True" si ya existe el valor en la grilla.
        /// </summary>
        /// <param name="column">Parámetro--> Referencia al nombre de la columna</param>
        /// <param name="match">Parámetro--> Referencia al valor de la columna</param>
        /// <param name="grid"></param>
        /// <returns></returns>
        public static bool ValidateExistDataOnGrid(string column, string match, CustomControls.CustomGrid grid)
        {

            if (grid.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in grid.Rows)
                {
                    if (row.Cells[column].Value.ToString().Equals(match))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
