﻿namespace Autonomo.CustomTemplate
{
    partial class CrudSingleGrid
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CrudSingleGrid));
            this.CustomContainer = new System.Windows.Forms.Panel();
            this.PanelGrid = new System.Windows.Forms.Panel();
            this.PanelBottom = new System.Windows.Forms.Panel();
            this.FormDetailData = new System.Windows.Forms.Label();
            this.FormEventHistory = new System.Windows.Forms.Label();
            this.BottomLine = new System.Windows.Forms.Label();
            this.PanelTop = new System.Windows.Forms.Panel();
            this.PanelFinder = new System.Windows.Forms.Panel();
            this.txFilter = new Autonomo.CustomControls.FlatFindText();
            this.btnExport = new Autonomo.CustomControls.CustomButton();
            this.btnNew = new Autonomo.CustomControls.CustomButton();
            this.TopLine = new System.Windows.Forms.Label();
            this.SpaceFinder = new System.Windows.Forms.Label();
            this.PanelTitle = new System.Windows.Forms.Panel();
            this.FormTitle = new System.Windows.Forms.Label();
            this.FormCloseControl = new System.Windows.Forms.Label();
            this.CustomContainer.SuspendLayout();
            this.PanelBottom.SuspendLayout();
            this.PanelTop.SuspendLayout();
            this.PanelFinder.SuspendLayout();
            this.PanelTitle.SuspendLayout();
            this.SuspendLayout();
            // 
            // CustomContainer
            // 
            this.CustomContainer.Controls.Add(this.PanelGrid);
            this.CustomContainer.Controls.Add(this.PanelBottom);
            this.CustomContainer.Controls.Add(this.PanelTop);
            this.CustomContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CustomContainer.Location = new System.Drawing.Point(0, 0);
            this.CustomContainer.Name = "CustomContainer";
            this.CustomContainer.Size = new System.Drawing.Size(800, 450);
            this.CustomContainer.TabIndex = 0;
            // 
            // PanelGrid
            // 
            this.PanelGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelGrid.Location = new System.Drawing.Point(0, 72);
            this.PanelGrid.Name = "PanelGrid";
            this.PanelGrid.Size = new System.Drawing.Size(800, 345);
            this.PanelGrid.TabIndex = 1;
            // 
            // PanelBottom
            // 
            this.PanelBottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            this.PanelBottom.Controls.Add(this.FormDetailData);
            this.PanelBottom.Controls.Add(this.FormEventHistory);
            this.PanelBottom.Controls.Add(this.BottomLine);
            this.PanelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelBottom.Location = new System.Drawing.Point(0, 417);
            this.PanelBottom.Name = "PanelBottom";
            this.PanelBottom.Size = new System.Drawing.Size(800, 33);
            this.PanelBottom.TabIndex = 2;
            // 
            // FormDetailData
            // 
            this.FormDetailData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            this.FormDetailData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FormDetailData.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormDetailData.Location = new System.Drawing.Point(0, 2);
            this.FormDetailData.Name = "FormDetailData";
            this.FormDetailData.Size = new System.Drawing.Size(704, 31);
            this.FormDetailData.TabIndex = 2;
            this.FormDetailData.Text = "Detail data";
            this.FormDetailData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FormEventHistory
            // 
            this.FormEventHistory.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            this.FormEventHistory.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FormEventHistory.Dock = System.Windows.Forms.DockStyle.Right;
            this.FormEventHistory.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormEventHistory.Image = ((System.Drawing.Image)(resources.GetObject("FormEventHistory.Image")));
            this.FormEventHistory.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.FormEventHistory.Location = new System.Drawing.Point(704, 2);
            this.FormEventHistory.Name = "FormEventHistory";
            this.FormEventHistory.Size = new System.Drawing.Size(96, 31);
            this.FormEventHistory.TabIndex = 3;
            this.FormEventHistory.Text = "     Events";
            this.FormEventHistory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // BottomLine
            // 
            this.BottomLine.BackColor = System.Drawing.SystemColors.Highlight;
            this.BottomLine.Dock = System.Windows.Forms.DockStyle.Top;
            this.BottomLine.Location = new System.Drawing.Point(0, 0);
            this.BottomLine.Name = "BottomLine";
            this.BottomLine.Size = new System.Drawing.Size(800, 2);
            this.BottomLine.TabIndex = 4;
            // 
            // PanelTop
            // 
            this.PanelTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            this.PanelTop.Controls.Add(this.PanelFinder);
            this.PanelTop.Controls.Add(this.PanelTitle);
            this.PanelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelTop.Location = new System.Drawing.Point(0, 0);
            this.PanelTop.Name = "PanelTop";
            this.PanelTop.Size = new System.Drawing.Size(800, 72);
            this.PanelTop.TabIndex = 0;
            // 
            // PanelFinder
            // 
            this.PanelFinder.Controls.Add(this.txFilter);
            this.PanelFinder.Controls.Add(this.btnExport);
            this.PanelFinder.Controls.Add(this.btnNew);
            this.PanelFinder.Controls.Add(this.TopLine);
            this.PanelFinder.Controls.Add(this.SpaceFinder);
            this.PanelFinder.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelFinder.Location = new System.Drawing.Point(0, 33);
            this.PanelFinder.Name = "PanelFinder";
            this.PanelFinder.Size = new System.Drawing.Size(800, 39);
            this.PanelFinder.TabIndex = 0;
            // 
            // txFilter
            // 
            this.txFilter.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txFilter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            this.txFilter.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txFilter.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txFilter.ColorLine = System.Drawing.Color.Gray;
            this.txFilter.ColorText = System.Drawing.SystemColors.WindowText;
            this.txFilter.ColorTitle = System.Drawing.Color.Gray;
            this.txFilter.Dock = System.Windows.Forms.DockStyle.Right;
            this.txFilter.DockIcon = System.Windows.Forms.DockStyle.Right;
            this.txFilter.FontText = new System.Drawing.Font("Verdana", 11F);
            this.txFilter.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txFilter.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txFilter.ImageIcon")));
            this.txFilter.Location = new System.Drawing.Point(530, 4);
            this.txFilter.MaterialStyle = false;
            this.txFilter.MaxLength = 32767;
            this.txFilter.MultiLineText = false;
            this.txFilter.Name = "txFilter";
            this.txFilter.ObjectArray = null;
            this.txFilter.PasswordChar = '\0';
            this.txFilter.Placeholder = "Buscar...";
            this.txFilter.PlaceHolderHeight = 6;
            this.txFilter.ReadOnly = false;
            this.txFilter.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txFilter.Size = new System.Drawing.Size(270, 33);
            this.txFilter.SizeLine = 2;
            this.txFilter.StringArray = null;
            this.txFilter.TabIndex = 0;
            this.txFilter.TextId = "";
            this.txFilter.Title = "";
            this.txFilter.VisibleIcon = true;
            this.txFilter.VisibleTitle = false;
            // 
            // btnExport
            // 
            this.btnExport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExport.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnExport.FlatAppearance.BorderSize = 0;
            this.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExport.Font = new System.Drawing.Font("Verdana", 10F);
            this.btnExport.Image = ((System.Drawing.Image)(resources.GetObject("btnExport.Image")));
            this.btnExport.Location = new System.Drawing.Point(104, 4);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(109, 33);
            this.btnExport.TabIndex = 1;
            this.btnExport.Text = "Export to";
            this.btnExport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExport.UseVisualStyleBackColor = true;
            // 
            // btnNew
            // 
            this.btnNew.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNew.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnNew.FlatAppearance.BorderSize = 0;
            this.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNew.Font = new System.Drawing.Font("Verdana", 10F);
            this.btnNew.Image = ((System.Drawing.Image)(resources.GetObject("btnNew.Image")));
            this.btnNew.Location = new System.Drawing.Point(0, 4);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(104, 33);
            this.btnNew.TabIndex = 1;
            this.btnNew.Text = "New data";
            this.btnNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNew.UseVisualStyleBackColor = true;
            // 
            // TopLine
            // 
            this.TopLine.BackColor = System.Drawing.SystemColors.Highlight;
            this.TopLine.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TopLine.Location = new System.Drawing.Point(0, 37);
            this.TopLine.Name = "TopLine";
            this.TopLine.Size = new System.Drawing.Size(800, 2);
            this.TopLine.TabIndex = 3;
            // 
            // SpaceFinder
            // 
            this.SpaceFinder.Dock = System.Windows.Forms.DockStyle.Top;
            this.SpaceFinder.Location = new System.Drawing.Point(0, 0);
            this.SpaceFinder.Name = "SpaceFinder";
            this.SpaceFinder.Size = new System.Drawing.Size(800, 4);
            this.SpaceFinder.TabIndex = 4;
            // 
            // PanelTitle
            // 
            this.PanelTitle.Controls.Add(this.FormTitle);
            this.PanelTitle.Controls.Add(this.FormCloseControl);
            this.PanelTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelTitle.Location = new System.Drawing.Point(0, 0);
            this.PanelTitle.Name = "PanelTitle";
            this.PanelTitle.Size = new System.Drawing.Size(800, 33);
            this.PanelTitle.TabIndex = 0;
            // 
            // FormTitle
            // 
            this.FormTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FormTitle.Font = new System.Drawing.Font("Verdana", 13F);
            this.FormTitle.Location = new System.Drawing.Point(0, 0);
            this.FormTitle.Name = "FormTitle";
            this.FormTitle.Size = new System.Drawing.Size(765, 33);
            this.FormTitle.TabIndex = 0;
            this.FormTitle.Text = "Title";
            this.FormTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.FormTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FormTitle_MouseDown);
            // 
            // FormCloseControl
            // 
            this.FormCloseControl.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FormCloseControl.Dock = System.Windows.Forms.DockStyle.Right;
            this.FormCloseControl.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormCloseControl.Location = new System.Drawing.Point(765, 0);
            this.FormCloseControl.Name = "FormCloseControl";
            this.FormCloseControl.Size = new System.Drawing.Size(35, 33);
            this.FormCloseControl.TabIndex = 1;
            this.FormCloseControl.Text = "X";
            this.FormCloseControl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CrudSingleGrid
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.CustomContainer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "CrudSingleGrid";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CrudSingleGrid";
            this.Load += new System.EventHandler(this.CrudSingleGrid_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CrudSingleGrid_KeyPress);
            this.CustomContainer.ResumeLayout(false);
            this.PanelBottom.ResumeLayout(false);
            this.PanelTop.ResumeLayout(false);
            this.PanelFinder.ResumeLayout(false);
            this.PanelTitle.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel CustomContainer;
        private System.Windows.Forms.Panel PanelBottom;
        public System.Windows.Forms.Label FormDetailData;
        public System.Windows.Forms.Label FormCloseControl;
        public System.Windows.Forms.Label FormTitle;
        public System.Windows.Forms.Panel PanelTop;
        public CustomControls.FlatFindText txFilter;
        public CustomControls.CustomButton btnExport;
        public CustomControls.CustomButton btnNew;
        public System.Windows.Forms.Label FormEventHistory;
        private System.Windows.Forms.Label TopLine;
        public System.Windows.Forms.Panel PanelGrid;
        public System.Windows.Forms.Label SpaceFinder;
        protected System.Windows.Forms.Panel PanelTitle;
        private System.Windows.Forms.Label BottomLine;
        public System.Windows.Forms.Panel PanelFinder;
    }
}