﻿
namespace Autonomo.CustomTemplate
{
    partial class GroupLotsPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainPanel = new System.Windows.Forms.Panel();
            this.SplitGroupContainer = new System.Windows.Forms.SplitContainer();
            this.PanelText = new System.Windows.Forms.Panel();
            this.PanelToControls = new System.Windows.Forms.Panel();
            this.MainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SplitGroupContainer)).BeginInit();
            this.SplitGroupContainer.Panel1.SuspendLayout();
            this.SplitGroupContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainPanel
            // 
            this.MainPanel.Controls.Add(this.SplitGroupContainer);
            this.MainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainPanel.Location = new System.Drawing.Point(0, 0);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(800, 450);
            this.MainPanel.TabIndex = 0;
            // 
            // SplitGroupContainer
            // 
            this.SplitGroupContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitGroupContainer.Location = new System.Drawing.Point(0, 0);
            this.SplitGroupContainer.Name = "SplitGroupContainer";
            // 
            // SplitGroupContainer.Panel1
            // 
            this.SplitGroupContainer.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            this.SplitGroupContainer.Panel1.Controls.Add(this.PanelToControls);
            this.SplitGroupContainer.Panel1.Controls.Add(this.PanelText);
            this.SplitGroupContainer.Size = new System.Drawing.Size(800, 450);
            this.SplitGroupContainer.SplitterDistance = 266;
            this.SplitGroupContainer.TabIndex = 0;
            // 
            // PanelText
            // 
            this.PanelText.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelText.Location = new System.Drawing.Point(0, 0);
            this.PanelText.Name = "PanelText";
            this.PanelText.Size = new System.Drawing.Size(266, 69);
            this.PanelText.TabIndex = 0;
            // 
            // PanelToControls
            // 
            this.PanelToControls.AutoScroll = true;
            this.PanelToControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelToControls.Location = new System.Drawing.Point(0, 69);
            this.PanelToControls.Name = "PanelToControls";
            this.PanelToControls.Size = new System.Drawing.Size(266, 381);
            this.PanelToControls.TabIndex = 1;
            // 
            // GroupLotsPanel
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.MainPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "GroupLotsPanel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GroupLotsPanel";
            this.MainPanel.ResumeLayout(false);
            this.SplitGroupContainer.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplitGroupContainer)).EndInit();
            this.SplitGroupContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel MainPanel;
        public System.Windows.Forms.SplitContainer SplitGroupContainer;
        public System.Windows.Forms.Panel PanelText;
        public System.Windows.Forms.Panel PanelToControls;
    }
}