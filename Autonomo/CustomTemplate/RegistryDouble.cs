﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Autonomo.CustomControls.HelperControl;

namespace Autonomo.CustomTemplate
{
    public partial class RegistryDouble : Form
    {
        public RegistryDouble()
        {
            InitializeComponent();
        }

        #region Theme
        public void ThemeStyle(Theme theme)
        {
            switch (theme)
            {
                case Theme.White:
                    {
                        Color topControlColor = Color.FromArgb(247, 249, 252);//247, 249, 252
                        Color topControlForeColor = Color.FromArgb(64, 64, 64);
                        SetButtonPanelAndTextColor(topControlColor, topControlForeColor, Theme.White);

                        Color backMainColor = Color.White;
                        Color backMainForeColor = Color.FromArgb(64, 64, 64);
                        Color detailForeColor = Color.DimGray;
                        SetPanelBackAndTextColor(backMainColor, backMainForeColor, detailForeColor);

                        SetGridColor(Color.White, Color.FromArgb(247, 249, 252),
                            Color.FromArgb(64,64,64), Color.FromArgb(64,64,64));
                        break;
                    }
                case Theme.Dark:
                    {
                        Color topControlColor = Color.FromArgb(16, 29, 37);
                        Color topControlForeColor = Color.Gainsboro;
                        SetButtonPanelAndTextColor(topControlColor, topControlForeColor, Theme.Dark);

                        Color backMainColor = Color.FromArgb(35, 45, 54);
                        Color backMainForeColor = Color.FromArgb(10, 140, 129); //Color.WhiteSmoke;
                        Color detailForeColor = Color.Gainsboro;
                        SetPanelBackAndTextColor(backMainColor, backMainForeColor, detailForeColor);

                        SetGridColor(Color.FromArgb(35, 45, 54), Color.FromArgb(16, 29, 37),
                            Color.Gainsboro, Color.WhiteSmoke);
                        SetControlColor();//Colocar parametros si se va a usar en otros temas.
                        break;
                    }
                case Theme.BlueDark:
                    {

                        break;
                    }
                case Theme.Green:
                    {

                        break;
                    }
            }
        }
        private void SetControlColor()
        {
            foreach (System.Windows.Forms.Control c in pnlTopControl.Controls)
            {
                if (c is Autonomo.CustomControls.FlatTextBox)
                {
                    //Personalizar colores si se va a usar más temas.
                    ((CustomControls.FlatTextBox)c).BackColor = Color.FromArgb(35, 45, 54);
                    ((CustomControls.FlatTextBox)c).lineMainColor = Color.FromArgb(10, 140, 129);
                    ((CustomControls.FlatTextBox)c).ColorLine = Color.FromArgb(10, 140, 129);
                    ((CustomControls.FlatTextBox)c).ColorText = Color.WhiteSmoke;
                    ((CustomControls.FlatTextBox)c).ColorFocus = Color.MintCream;
                    ((CustomControls.FlatTextBox)c).ColorTitle = Color.Gainsboro;
                }
                if (c is Autonomo.CustomControls.FlatFindText)
                {
                    //Personalizar colores si se va a usar más temas.
                    ((CustomControls.FlatFindText)c).BackColor = Color.FromArgb(35, 45, 54);
                    ((CustomControls.FlatFindText)c).lineMainColor = Color.FromArgb(10, 140, 129);
                    ((CustomControls.FlatFindText)c).ColorLine = Color.FromArgb(10, 140, 129);
                    ((CustomControls.FlatFindText)c).ColorText = Color.WhiteSmoke;
                    ((CustomControls.FlatFindText)c).ColorFocus = Color.MintCream;
                    ((CustomControls.FlatFindText)c).ColorTitle = Color.Gainsboro;
                }
                if (c is Autonomo.CustomControls.FlatComboBox)
                {
                    //Personalizar colores si se va a usar más temas.
                    ((CustomControls.FlatComboBox)c).BackColor = Color.FromArgb(35, 45, 54);
                    ((CustomControls.FlatComboBox)c).lineMainColor = Color.FromArgb(10, 140, 129);
                    ((CustomControls.FlatComboBox)c).ColorLine = Color.FromArgb(10, 140, 129);
                    ((CustomControls.FlatComboBox)c).ColorText = Color.WhiteSmoke;
                    ((CustomControls.FlatComboBox)c).ColorFocus = Color.MintCream;
                    ((CustomControls.FlatComboBox)c).ColorTitle = Color.Gainsboro;
                }
                if (c is Autonomo.CustomControls.FlatDateTime)
                {
                    //Personalizar colores si se va a usar más temas.
                    ((CustomControls.FlatDateTime)c).BackColor = Color.FromArgb(35, 45, 54);
                    ((CustomControls.FlatDateTime)c).lineMainColor = Color.FromArgb(10, 140, 129);
                    ((CustomControls.FlatDateTime)c).ColorLine = Color.FromArgb(10, 140, 129);
                    ((CustomControls.FlatDateTime)c).ColorText = Color.WhiteSmoke;
                    ((CustomControls.FlatDateTime)c).ColorFocus = Color.MintCream;
                    ((CustomControls.FlatDateTime)c).ColorTitle = Color.Gainsboro;
                }
            }
        }
        private void SetGridColor(Color backColor, Color headBackColor, Color headFontColor
            , Color bodyFontColor)
        {
            // Solo en el panel de nombre pnlGrid se insertará la Grilla... casi contrario no se reflejará efectos.
            foreach (System.Windows.Forms.Control c in ContenedorDoble.Panel1.Controls)
            {
                if (c is Autonomo.CustomControls.CustomGrid)
                {
                    ((CustomControls.CustomGrid)c).BackgroundColor = backColor;
                    ((CustomControls.CustomGrid)c).HeaderColor = headBackColor;
                    ((CustomControls.CustomGrid)c).HeaderForeColor = headFontColor;
                    ((CustomControls.CustomGrid)c).BodyForeColor = bodyFontColor;
                    ((CustomControls.CustomGrid)c).CellStyleBackColor = backColor;
                }
            }
            foreach (System.Windows.Forms.Control c in ContenedorDoble.Panel2.Controls)
            {
                if (c is Autonomo.CustomControls.CustomGrid)
                {
                    ((CustomControls.CustomGrid)c).BackgroundColor = backColor;
                    ((CustomControls.CustomGrid)c).HeaderColor = headBackColor;
                    ((CustomControls.CustomGrid)c).HeaderForeColor = headFontColor;
                    ((CustomControls.CustomGrid)c).BodyForeColor = bodyFontColor;
                    ((CustomControls.CustomGrid)c).CellStyleBackColor = backColor;
                }
            }
        }

        private void SetPanelBackAndTextColor(Color panelColor, Color titleForeColor, Color detailForeColor)
        {            
            pnlContenedor.BackColor = panelColor;
            Title.ForeColor = titleForeColor;
            DescriptionData.ForeColor = detailForeColor;

        }
        private void SetButtonPanelAndTextColor(Color panelColor, Color foreColor, Theme theme)
        {
            ContenedorDoble.BackColor = panelColor;
            pnlTopButton.BackColor = panelColor;
            TopLine.BackColor = panelColor;
            BottomLine.BackColor = panelColor;

            btnNuevo.ForeColor = foreColor;
            btnEditar.ForeColor = foreColor;
            btnEliminar.ForeColor = foreColor;
            btnExportar.ForeColor = foreColor;
            btnCommand1.ForeColor = foreColor;
            btnCommand2.ForeColor = foreColor;
            btnCommand3.ForeColor = foreColor;

            // Validar si los temas son oscuros o claros.
            if (theme == Theme.Dark)// Si el fondo del panel es oscuro:
            {
                btnNuevo.Image = Properties.Resources.Add20;
                btnEditar.Image = Properties.Resources.Edit20;
                btnEliminar.Image = Properties.Resources.Delete20;
                btnExportar.Image = Properties.Resources.Excel20;
            }
        }
        #endregion

        public void SetLineColor(Color color)
        {
            TopLine.BackColor = color;
            BottomLine.BackColor = color;
        }

        private void RegistryDouble_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r' || e.KeyChar == (char)Keys.Escape)
                e.Handled = true;
        }

        private void RegistryDouble_Load(object sender, EventArgs e)
        {

        }
    }
}
