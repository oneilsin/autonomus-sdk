﻿using Autonomo.Class;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using static Autonomo.Class.Enumerable;

namespace Autonomo.CustomTemplate
{
    public partial class MenuTopPanel : Form
    {
        Color _backTitle;
        Color _backBar;
        Color _backBorder;
        bool _moveForm, _mf;
        string _sesion;
        bool _shadow;
        ScreamState _scream;
        int _locationX, _locationY;
        Size _size;
        Color _controlForeColor;
        Font _profileFont;
        Cookie _cookie;
        Image _imagePhoto;
        public MenuTopPanel()
        {
            InitializeComponent();
            //if (_shadow)
            //    (new Class.DropShadow()).ApplyShadows(this);

            _backTitle = TopPanel.BackColor;
            _backBar = BarPanel.BackColor;
            _backBorder = izquierda.BackColor;
            _moveForm = _mf = false;
            _sesion = "user-name";
            _shadow = false;
            _scream = ScreamState.Normal;
            _locationX = this.Location.X;
            _locationY = this.Location.Y;
            _size = this.Size;
            t.BackColor = _backTitle;
            t.ForeColor = _backTitle;
            _profileFont = profile.Font;
            _cookie = new Cookie();
            profile.Click += new EventHandler(profile_Click);
            help.Click += new EventHandler(help_Click);
            about.Click += new EventHandler(about_Click);
            logOut.Click += new EventHandler(logOut_Click);
            _imagePhoto = Properties.Resources.Photo;
        }

        #region Style profile labels
        private void profile_MouseEnter(object sender, EventArgs e)
        {
            profile.Font = new Font(_profileFont, FontStyle.Bold);
        }

        private void profile_MouseLeave(object sender, EventArgs e)
        {
            profile.Font = new Font(_profileFont, FontStyle.Regular);
        }

        private void help_MouseEnter(object sender, EventArgs e)
        {
            help.Font = new Font(_profileFont, FontStyle.Bold);
        }

        private void help_MouseLeave(object sender, EventArgs e)
        {
            help.Font = new Font(_profileFont, FontStyle.Regular);
        }

        private void about_MouseEnter(object sender, EventArgs e)
        {
            about.Font = new Font(_profileFont, FontStyle.Bold);
        }

        private void about_MouseLeave(object sender, EventArgs e)
        {
            about.Font = new Font(_profileFont, FontStyle.Regular);
        }

        private void logOut_MouseEnter(object sender, EventArgs e)
        {
            logOut.Font = new Font(_profileFont, FontStyle.Bold);
        }

        private void logOut_MouseLeave(object sender, EventArgs e)
        {
            logOut.Font = new Font(_profileFont, FontStyle.Regular);
        }
        #endregion

        #region Events Button Click
        /// <summary>
        /// Evetno ButtonClick que ejecuta cieta instrucción 
        /// </summary>
        /// 
        [Category("Autonomo properties")]
        public event EventHandler ProfileClick;
        void profile_Click(object sender, EventArgs e)
        {
            if (ProfileClick != null) ProfileClick(sender, e);
            _profile.Visible = false;
        }

        /// <summary>
        /// Evetno ButtonClick que ejecuta cieta instrucción 
        /// </summary>
        /// 
        [Category("Autonomo properties")]
        public event EventHandler HelpClick;
        void help_Click(object sender, EventArgs e)
        {
            if (HelpClick != null) HelpClick(sender, e);
            _profile.Visible = false;
        }

        /// <summary>
        /// Evetno ButtonClick que ejecuta cieta instrucción 
        /// </summary>
        /// 
        [Category("Autonomo properties")]
        public event EventHandler AboutClick;
        void about_Click(object sender, EventArgs e)
        {
            if (AboutClick != null) AboutClick(sender, e);
            _profile.Visible = false;
        }

        /// <summary>
        /// Evetno ButtonClick que ejecuta cieta instrucción 
        /// </summary>
        /// 
        [Category("Autonomo properties")]
        public event EventHandler LogOutClick;
        void logOut_Click(object sender, EventArgs e)
        {
            if (LogOutClick != null) LogOutClick(sender, e);
            _profile.Visible = false;
        }
        #endregion

        [Category("Autonomo properties")]
        [Description("Change: True = Move formulary and False: Disabled move formulary.")]
        public Image ImagePhoto
        {
            get { return _imagePhoto; }
            set { 
                _imagePhoto = value;
                picture.Image = _imagePhoto;
            }
        }

        [Category("Autonomo properties")]
        [Description("Administra la altura del panel superior del formulario.")]
        public int HeightTopPanel
        {
            get { return TopPanel.Height; }
            set
            {
                TopPanel.Height = value;
            }
        }

        


        [Category("Autonomo properties")]
        [Description("Change: True = Move formulary and False: Disabled move formulary.")]
        public BorderStyle BorderNewStyle
        {
            get { return MainPanel.BorderStyle; }
            set { MainPanel.BorderStyle = value; }
        }

        [Category("Autonomo properties")]
        [Description("Set: Show status, connection to the application. Write on Load();")]
        public void SetConnectionStatus(Cookie cookie)
        {
            _cookie = cookie;
            abajo.Text = string.Format(abajo.Text, _cookie.Module, _cookie.Server, _cookie.DataBase, _cookie.UserFullName);
            WhoIsIn = _cookie.UserFullName;
            string r = "Role {0}";
            role.Text = string.Format(r, _cookie.Role[0].Trim());
            picture.Image = _cookie.Profile;
        }

        [Category("Autonomo properties")]
        [Description("Change: True = Move formulary and False: Disabled move formulary.")]
        public bool MoveFormumary
        {
            get { return _moveForm; }
            set
            {
                _moveForm = value;
                _mf = _moveForm;
            }
        }

        [Category("Autonomo properties")]
        [Description("Change: True = Move formulary and False: Disabled move formulary.")]
        public Color ForeColorControl
        {
            get { return _controlForeColor; }
            set
            {
                _controlForeColor = value;

                sesion.ForeColor = _controlForeColor;
                Maximizar.ForeColor = _controlForeColor;
                Salir.ForeColor = _controlForeColor;
                Minimizar.ForeColor = _controlForeColor;
            }
        }

        [Category("Autonomo properties")]
        [Description("BackColor: Set title back-color.")]
        public Color BackColorTitle
        {
            get { return _backTitle; }
            set
            {
                _backTitle = value;
                TopPanel.BackColor = _backTitle;
                Logo.BackColor = _backTitle;
                Company.BackColor = _backTitle;
                sesion.BackColor = _backTitle;
                TopPanel.BackColor = _backTitle;
                t.BackColor = _backTitle;
                t.ForeColor = _backTitle;
            }
        }

        [Category("Autonomo properties")]
        [Description("BackColor: Set title navbar-color.")]
        public Color BackColorBar
        {
            get { return _backBar; }
            set
            {
                _backBar = value;
                BarPanel.BackColor = _backBar;
                _space.BackColor = _backBar;
            }
        }

        [Category("Autonomo properties")]
        [Description("BackColor: Set title border-color.")]
        public Color BackColorBorder
        {
            get { return _backBorder; }
            set
            {
                _backBorder = value;
                arriba.BackColor = _backBorder;
                derecha.BackColor = _backBorder;
                abajo.BackColor = _backBorder;
                izquierda.BackColor = _backBorder;              
            }
        }

        [Category("Autonomo properties")]
        [Description("BackColor: Set title border-color.")]
        public string WhoIsIn
        {
            get { return _sesion; }
            set
            {
                _sesion = value; //Welcome: Autonomus SDK- JSD
                sesion.Text = string.Format("      Welcome: {0}", (_sesion.Length > 18 ? _sesion.Substring(0, 18) : _sesion));
                userName.Text = _cookie.UserFullName;                
            }
        }

        [Category("Autonomo properties")]
        [Description("Shadow: Set shadow style on form.")]
        public bool Shadow
        {
            get { return _shadow; }
            set
            {
                _shadow = value;
            }
        }

        [Category("Autonomo properties")]
        [Description("State: Maximize and restore Windows-State.")]
        public ScreamState ScreamState
        {
            get { return _scream; }
            set
            {
                _scream = value;
            }
        }

        [Category("Autonomo properties")]
        [Description("Size: Set custom size form.")]
        public Size SizeCustom
        {
            get { return _size; }
            set
            {
                _size = value;
            }
        }
        private void MenuTopPanel_Load(object sender, EventArgs e)
        {
            if (_scream == ScreamState.Maximize)
            {
                this.Location = Screen.PrimaryScreen.WorkingArea.Location;
                this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            }

            _locationX = this.Location.X;
            _locationY = this.Location.Y;
            t.Focus();

            locationPopUp();
        }
        void locationPopUp()
        {
            int _x = sesion.Location.X + 161;
            int _y = sesion.Location.Y + 36;
            _profile.Location = new Point(_x, _y);
        }

        private void TopPanel_MouseDown(object sender, MouseEventArgs e)
        {
            if (_moveForm)
                Class.MoveObject.MoveForm(this);
        }

        private void Salir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Minimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void MenuTopPanel_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                //string result = Autonomus.Class.Message.MessageBox(3,
                //"¡Hasta pronto!. No hay procesos pentientes, puede salir de la Aplicación sin problemas." + Environment.NewLine + Environment.NewLine +
                //"¿Estás seguro de continuar?");
                if ("Yes" == "Yes")
                { Application.Exit(); }
                else
                { e.Cancel.Equals(true); }
            }
        }

        private void MenuTopPanel_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r' || e.KeyChar == (char)Keys.Escape)
                e.Handled = true;
        }

        private void tmrDateTime_Tick(object sender, EventArgs e)
        {
            time.Text = $"{DateTime.Now.ToLongDateString()}  {DateTime.Now.ToLongTimeString()}";
        }

        private void Maximizar_Click(object sender, EventArgs e)
        {
            switch (_scream)
            {
                case ScreamState.Normal:
                    {
                        maximScream();
                        break;
                    }
                case ScreamState.Maximize:
                    {
                        restoreScream();
                        break;
                    }
            }
        }

        private void sesion_Click(object sender, EventArgs e)
        {
            _profile.Visible = true;
            //            MessageBox.Show($"panel...   shadow:{_shadow}");
        }

        private void PopUpClose_Click(object sender, EventArgs e)
        {
            _profile.Visible = false;
        }

        private void TopPanel_DoubleClick(object sender, EventArgs e)
        {
            switch (_scream)
            {
                case ScreamState.Normal:
                    {
                        maximScream();
                        break;
                    }
                case ScreamState.Maximize:
                    {
                        restoreScream();
                        break;
                    }
            }
        }

        void maximScream()
        {
            _locationX = this.Location.X;
            _locationY = this.Location.Y;
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            this.Location = Screen.PrimaryScreen.WorkingArea.Location;

            _scream = ScreamState.Maximize;

            _moveForm = _moveForm ? false : _mf;

            locationPopUp();
        }
        void restoreScream()
        {
            this.Size = _size;
            this.Location = new Point(_locationX, _locationY);
            _scream = ScreamState.Normal;

            _moveForm = _moveForm ? true : _mf;

            locationPopUp();
        }
    }
}
