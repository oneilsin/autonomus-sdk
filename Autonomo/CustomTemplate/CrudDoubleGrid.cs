﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Autonomo.CustomControls.HelperControl;

namespace Autonomo.CustomTemplate
{
    public partial class CrudDoubleGrid : Form
    {
        Color _lineColor;
        Color _lineBottom;
        bool _moveForm;
        public CrudDoubleGrid()
        {
            InitializeComponent();
            _lineColor = PanelTop.BackColor;
            TopLine.BackColor = _lineColor;
            _lineBottom = PanelBottom.BackColor;
            BottomLine.BackColor = _lineBottom;
            _moveForm = false;
        }

        [Category("Autonomo properties")]
        [Description("Change: True = Move formulary and False: Disabled move formulary.")]
        public bool MoveFormumary
        {
            get { return _moveForm; }
            set { _moveForm = value; }
        }

        [Category("Autonomo properties")]
        public Color GridLineColor
        {
            get { return _lineColor; }
            set
            {
                _lineColor = value;
                TopLine.BackColor = _lineColor;
            }
        }

        [Category("Autonomo properties")]
        public Color DetailLineColor
        {
            get { return _lineBottom; }
            set
            {
                _lineBottom = value;
                BottomLine.BackColor = _lineBottom;
            }
        }

        //[Category("Autonomo properties")]
        //public Color SeparatorLineColor
        //{
        //    get { return _separator.BackColor; }
        //    set
        //    {
        //        _separator.BackColor = value;
        //    }
        //}

        private void CrudDoubleGrid_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r' || e.KeyChar == (char)Keys.Escape)
                e.Handled = true;
        }

        private void CrudDoubleGrid_Load(object sender, EventArgs e)
        {
            txFilter.Focus();
        }

        private void FormTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (_moveForm)
                Autonomo.Class.MoveObject.MoveForm(this);
        }

        #region Theme
        public void ThemeStyle(Theme theme)
        {
            switch (theme)
            {
                case Theme.White:
                    {
                        Color topControlColor = Color.FromArgb(247, 249, 252);
                        Color topControlForeColor = Color.FromArgb(64, 64, 64);
                        SetButtonPanelAndTextColor(topControlColor, topControlForeColor, Theme.White);

                        Color backMainColor = Color.White;
                        Color backMainForeColor = Color.FromArgb(64, 64, 64);
                        Color detailForeColor = Color.DimGray;
                        SetPanelBackAndTextColor(backMainColor, backMainForeColor, detailForeColor);

                        SetGridColor(Color.White, Color.FromArgb(247, 249, 252),
                            Color.Black, Color.Black);
                        break;
                    }
                case Theme.Dark:
                    {
                        Color topControlColor = Color.FromArgb(16, 29, 37);
                        Color topControlForeColor = Color.Gainsboro;
                        SetButtonPanelAndTextColor(topControlColor, topControlForeColor, Theme.Dark);

                        Color backMainColor = Color.FromArgb(35, 45, 54);
                        Color backMainForeColor = Color.FromArgb(10, 140, 129); //Color.WhiteSmoke;
                        Color detailForeColor = Color.Gainsboro;
                        SetPanelBackAndTextColor(backMainColor, backMainForeColor, detailForeColor);

                        SetGridColor(Color.FromArgb(35, 45, 54), Color.FromArgb(16, 29, 37),
                            Color.Gainsboro, Color.WhiteSmoke);
                        SetControlColor();//Colocar parametros si se va a usar en otros temas.
                        break;
                    }
                case Theme.BlueDark:
                    {

                        break;
                    }
                case Theme.Green:
                    {

                        break;
                    }
            }
        }
        private void SetControlColor()
        {
            foreach (System.Windows.Forms.Control c in PanelFinder.Controls)
            {
                if (c is Autonomo.CustomControls.FlatTextBox)
                {
                    //Personalizar colores si se va a usar más temas.
                    ((CustomControls.FlatTextBox)c).BackColor = Color.FromArgb(35, 45, 54);
                    ((CustomControls.FlatTextBox)c).lineMainColor = Color.FromArgb(10, 140, 129);
                    ((CustomControls.FlatTextBox)c).ColorLine = Color.FromArgb(10, 140, 129);
                    ((CustomControls.FlatTextBox)c).ColorText = Color.WhiteSmoke;
                    ((CustomControls.FlatTextBox)c).ColorFocus = Color.MintCream;
                    ((CustomControls.FlatTextBox)c).ColorTitle = Color.Gainsboro;
                }
                if (c is Autonomo.CustomControls.FlatFindText)
                {
                    //Personalizar colores si se va a usar más temas.
                    ((CustomControls.FlatFindText)c).BackColor = Color.FromArgb(35, 45, 54);
                    ((CustomControls.FlatFindText)c).lineMainColor = Color.FromArgb(10, 140, 129);
                    ((CustomControls.FlatFindText)c).ColorLine = Color.FromArgb(10, 140, 129);
                    ((CustomControls.FlatFindText)c).ColorText = Color.WhiteSmoke;
                    ((CustomControls.FlatFindText)c).ColorFocus = Color.MintCream;
                    ((CustomControls.FlatFindText)c).ColorTitle = Color.Gainsboro;
                }
                if (c is Autonomo.CustomControls.FlatComboBox)
                {
                    //Personalizar colores si se va a usar más temas.
                    ((CustomControls.FlatComboBox)c).BackColor = Color.FromArgb(35, 45, 54);
                    ((CustomControls.FlatComboBox)c).lineMainColor = Color.FromArgb(10, 140, 129);
                    ((CustomControls.FlatComboBox)c).ColorLine = Color.FromArgb(10, 140, 129);
                    ((CustomControls.FlatComboBox)c).ColorText = Color.WhiteSmoke;
                    ((CustomControls.FlatComboBox)c).ColorFocus = Color.MintCream;
                    ((CustomControls.FlatComboBox)c).ColorTitle = Color.Gainsboro;
                }
                if (c is Autonomo.CustomControls.FlatDateTime)
                {
                    //Personalizar colores si se va a usar más temas.
                    ((CustomControls.FlatDateTime)c).BackColor = Color.FromArgb(35, 45, 54);
                    ((CustomControls.FlatDateTime)c).lineMainColor = Color.FromArgb(10, 140, 129);
                    ((CustomControls.FlatDateTime)c).ColorLine = Color.FromArgb(10, 140, 129);
                    ((CustomControls.FlatDateTime)c).ColorText = Color.WhiteSmoke;
                    ((CustomControls.FlatDateTime)c).ColorFocus = Color.MintCream;
                    ((CustomControls.FlatDateTime)c).ColorTitle = Color.Gainsboro;
                }
            }
        }
        private void SetGridColor(Color backColor, Color headBackColor, Color headFontColor
            , Color bodyFontColor)
        {
            // Solo en el panel de nombre pnlGrid se insertará la Grilla... casi contrario no se reflejará efectos.
            foreach (System.Windows.Forms.Control c in splitGridContainer.Controls)
            {
                if (c is Autonomo.CustomControls.CustomGrid)
                {
                    ((CustomControls.CustomGrid)c).BackgroundColor = backColor;
                    ((CustomControls.CustomGrid)c).HeaderColor = headBackColor;
                    ((CustomControls.CustomGrid)c).HeaderForeColor = headFontColor;
                    ((CustomControls.CustomGrid)c).BodyForeColor = bodyFontColor;
                    ((CustomControls.CustomGrid)c).CellStyleBackColor = backColor;
                }
            }
        }

        private void SetPanelBackAndTextColor(Color panelColor, Color titleForeColor, Color detailForeColor)
        {
            CustomContainer.BackColor = panelColor;
            FormTitle.ForeColor = titleForeColor;
            FormDetailData.ForeColor = detailForeColor;

        }
        private void SetButtonPanelAndTextColor(Color panelColor, Color foreColor, Theme theme)
        {
            PanelFinder.BackColor = panelColor;
            TopLine.BackColor = panelColor;
            BottomLine.BackColor = panelColor;

            btnNew.ForeColor = foreColor;
            btnExport.ForeColor = foreColor;
           

            // Validar si los temas son oscuros o claros.
            if (theme == Theme.Dark)// Si el fondo del panel es oscuro:
            {
                btnNew.Image = Properties.Resources.Add20;
                btnExport.Image = Properties.Resources.Excel20;
            }
        }
        #endregion

    }
}
