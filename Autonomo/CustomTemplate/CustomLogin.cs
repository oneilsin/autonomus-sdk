﻿using Autonomo.Class;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autonomo.CustomTemplate
{
    public partial class CustomLogin : Form
    {
        /*[Category("Autonomo properties")]
        [Description("Atributo que permite controlar las versiones, es necesario establecer la versión desde la app. " +
            "Tener en cuenta que, esta vesión se extrae desde la base de datos.")]
        public string OldAppVersion;*/


        string _newVersion;// Se refiere a la versión que está implementada en la APP.
        Cookie _cookie; // Version::: Se refiere a la versión que está registrado en la DB.
        [Category("Autonomo properties")]
        [Description("Lista de opciones para identificar el estado de la validación de accesos.")]
        public enum LogIn { Nothing, Success, Wrong }

        [Category("Autonomo properties")]
        [Description("Identificador del estado de la validación de accesos. " +
            "Es necesario cambiar el estado después de valdidar el acceso, en metodo (valdiar usuario y contraseña)")]
        public LogIn Login;


        [Description("Algun dato que sea necesario......")]
        public CustomLogin()
        {
            InitializeComponent();
            _newVersion = string.Empty;
            Login = LogIn.Nothing;
            _cookie = new Cookie();


        }

        #region Espacio Atributos

        [Category("Autonomo properties")]
        [Description("Atributo que permite establecer el título al formulario.")]
        public string Title
        {
            get { return _title.Text; }
            set { _title.Text = value; }
        }

        [Category("Autonomo properties")]
        [Description("Atributo que permite cambiar el color de fondo del espacio del titulo.")]
        public Color TitleBackColor
        {
            get { return _panelTitle.BackColor; }
            set { _panelTitle.BackColor = value;  btnClose.BackColor = value; }
        }

        [Category("Autonomo properties")]
        [Description("Atributo que permite cambiar el color del signo Cerrar X.")]
        public Color CloseForeColor
        {
            get { return btnClose.ForeColor; }
            set { btnClose.ForeColor = value;  }
        }

        [Category("Autonomo properties")]
        [Description("Atributo que permite cambiar la imagen/logotipo de la aplicación.")]
        public Image LogoImage
        {
            get { return _logo.Image; }
            set { _logo.Image = value; }
        }

        [Category("Autonomo properties")]
        [Description("Atributo que permite cambiar el color de fondo del logotipo de la aplicación.")]
        public Color LogoBackColor
        {
            get { return _logo.BackColor; }
            set { _logo.BackColor = value; }
        }

        [Category("Autonomo properties")]
        [Description("Atributo que permite cambiar la dimensión del panel Logo.")]
        public Size LogoSize { get { return _panelLogo.Size; } set { _panelLogo.Size = value; } }



        [Category("Autonomo properties")]
        [Description("Atributo que permite cambiar la descripción y detalle de la aplicación.")]
        public string Description
        {
            get { return _description.Text; }
            set { _description.Text = value; }
        }

        [Category("Autonomo properties")]
        [Description("Atributo que permite cambiar la fuente de la descripción.")]
        public Font DescriptionFont { get { return _description.Font; } set { _description.Font = value; } }
        

        [Category("Autonomo properties")]
        [Description("Atributo que permite cambiar el color de la Fuente.")]
        public Color DescriptFontColor { get { return _description.ForeColor; } set { _description.ForeColor = value; } }

        [Category("Autonomo properties")]
        [Description("Atributo que permite establecer la versión implementada.")]
        public string Version
        {
            get { return _newVersion; }
            set
            {
                _newVersion = value;
                _version.Text = $"versión: {_newVersion}";
            }
        }
        #endregion

        #region Espacio Metodos
        [Category("Autonomo properties")]
        [Description("Método que permite establecer los valores del usuario u otro dato después" +
            " de la validación de accesos. Trabaja con la clase Cookie")]
        public void LoadLoginInformation(Cookie cookie)
        {
            TextInfo myTI = new CultureInfo("en-US", false).TextInfo;
            _cookie.UserName = myTI.ToTitleCase(cookie.UserFullName.ToLower());
            //Nombre: JUAN PEREZ --> juan perez --> Juan Perez
            _cookie.Version = cookie.Version;
            _cookie.UserName = cookie.UserFullName;
        }

        public bool CompareVersion()
        {
            string oldVersion = this._cookie.Version; // del SQL o base de datos
            string newVersion = _newVersion; // de la aplicación.
            if (oldVersion == newVersion)
            {
                //
                return true;
            }
            else
            {
                // Pronto MessageBoxpersonalizado.
                string message = $"Versión actual del sistema: {oldVersion}, parece que hay una nueva versión. " +
                    $"Por favor, actualizar la aplicación.";
                MessageBox.Show(message);
                return false;
            }
        }

        void ShowModule()
        {
            if (!CompareVersion()) return;

            if (Login == LogIn.Success)
            {
                if (cbModulo.Items.Count > 1)
                {
                    _panelModule.Visible = true;
                    _panelModule.Dock = DockStyle.Fill;
                    _panelModule.BringToFront();

                    _panelAccess.Visible = false;
                    _userName.Text = _cookie.UserFullName;
                }
            }
        }


        void Cancel()
        {
            Application.Exit();
        }
        #endregion


        #region Espacio Eventos()
        [Category("Autonomo properties")]
        public event EventHandler JoinClick;
        void join_Click(object sender, EventArgs e)
        {
            if (JoinClick != null) JoinClick(sender, e);
        }

        #endregion
        private void btnClose_Click(object sender, EventArgs e)
        {
            Cancel();
        }

        private void CustomLogin_Load(object sender, EventArgs e)
        {
            //RoundObject.RoundButton(btnClose, 7, 7);
            _next.Click += new EventHandler(join_Click);
            _version.Text = $"version: {_newVersion}";
            _panelAccess.Dock = DockStyle.Fill;
            _panelAccess.BringToFront();
            _panelAccess.Visible = true;
        }

        private void _title_MouseDown(object sender, MouseEventArgs e)
        {
            MoveObject.MoveForm(this);
        }

        private void _logo_MouseDown(object sender, MouseEventArgs e)
        {
            MoveObject.MoveForm(this);
        }

        private void _previous_Click(object sender, EventArgs e)
        {
            txUsuario.Clear();
            txPassword.Clear();
            txUsuario.Enabled = true;
            txPassword.Enabled = true;
            txUsuario.ReadOnly = false;
            txPassword.ReadOnly = false;
            _userName.Text = "user";

            _panelModule.Visible = false;
            _panelModule.Dock = DockStyle.None;

            _panelAccess.Visible = true;
            _panelAccess.Dock = DockStyle.Fill;
            _panelAccess.BringToFront();
        }

        private void _next_Click(object sender, EventArgs e)
        {
            //mostrar la pantalla principal.
        }

        private void btnAccess_Click(object sender, EventArgs e)
        {
            if (Login == LogIn.Success)
            {
                ShowModule();
            }
        }

        private void CustomLogin_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r' || e.KeyChar == (char)Keys.Escape)
                e.Handled = true;
        }

        private void CustomLogin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape) { Cancel(); }
        }
    }
}
