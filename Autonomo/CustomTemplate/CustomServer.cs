﻿using Autonomo.Class;
using Autonomo.CustomControls;
using Autonomo.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Autonomo.Class.Enumerable;
using static Autonomo.CustomControls.HelperControl;

namespace Autonomo.CustomTemplate
{
    public partial class CustomServer : Form
    {
        public State StateForm;
        bool _moveForm;
        Color _topColor;
        List<TableModel> _model;
        string _fileName;
        string _nameConnection;
        public CustomServer(string fileName, string nameConnection, List<TableModel> table)
        {
            InitializeComponent();
            StateForm = State.Cancel;
            _moveForm = false;
            _topColor = PanelTop.BackColor;
            _model = new List<TableModel>();
            _model = table;
            _fileName = fileName;
            _nameConnection = nameConnection;
        }

        private void CustomServer_Load(object sender, EventArgs e)
        {

        }

        [Category("Autonomo properties")]
        [Description("Change: True = Move formulary and False: Disabled move formulary.")]
        public BorderStyle BorderNewStyle
        {
            get { return CustomContainer.BorderStyle; }
            set { CustomContainer.BorderStyle = value; }
        }

        [Category("Autonomo properties")]
        [Description("Change: True = Move formulary and False: Disabled move formulary.")]
        public bool MoveFormumary
        {
            get { return _moveForm; }
            set
            {
                _moveForm = value;
                if (value)
                    PanelTop.Cursor = Cursors.Hand;
                else
                    PanelTop.Cursor = Cursors.Default;
            }
        }

        [Category("Autonomo properties")]
        public Color TopBackColor
        {
            get { return _topColor; }
            set
            {
                _topColor = value;
                PanelTop.BackColor = _topColor;
            }
        }

        [Category("Autonomo properties")]
        public void ThemeStyle(Theme theme)
        {
            switch (theme)
            {
                case Theme.White:
                    break;
                case Theme.Dark:
                    break;
                case Theme.BlueDark:
                    break;
                case Theme.Green:
                    break;
                default:
                    break;
            }
        }

        [Category("Autonomo properties")]
        public void ConfigButton(Color buttonBackColor, Color buttonForeColor, int x, int y)
        {
            btnConnected.BackColor = buttonBackColor;
            btnConnected.ForeColor = buttonForeColor;
            Class.RoundObject.RoundButton(btnConnected, x, y);
        }

        public void Set()
        {
            this.Tag = "Get";
            StateForm = State.Success;
            this.Close();
        }

        private void Cancel()
        {
            this.Tag = "None";
            StateForm = State.Cancel;
            this.Close();
        }

        /// <summary>
        /// Estado del formulario, nos permite validar si se ha seleccionado los valores de la grilla 
        /// o si se ha cerrado el formulario sin seleccionar la data.
        /// </summary>
        [Category("Autonomo properties")]
        public bool StateFormulary
        {
            get
            {
                return StateForm == State.Success ? true : false;
            }
        }

        private void FormCloseControl_Click(object sender, EventArgs e)
        {
            Cancel();
        }

        private void CustomServer_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r' || e.KeyChar == (char)Keys.Escape)
                e.Handled = true;
        }

        private void CustomServer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape) { Cancel(); }
        }

        private void FormTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (_moveForm)
                Class.MoveObject.MoveForm(this);
        }

        private void txPassword_Leave(object sender, EventArgs e)
        {
            ValidateAccess();
        }

        private void txPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                ValidateAccess();
        }

        #region Manage Access

        void ValidateAccess()
        {
            var response = SqlContext.GetSqlConnection(cbData,
                new SqlModel()
                {
                    Server = txServer.Text.Trim(),
                    User = txUser.Text.Trim(),
                    Password = txPassword.Text.Trim()
                });

            if (response)
            {
                lbMessage.Text = "Conexión al servidor completado!";
                lbMessage.BackColor = Color.LightSeaGreen;
            }
            else
            {
                lbMessage.Text = "Conexión al servidor No completada!";
                lbMessage.BackColor = Color.LightSalmon;
            }
        }

        void SetFileConnection(SqlModel sql)
        {
            string path = Application.StartupPath + @"\" + _fileName;
            if (File.Exists(path))
            {
                File.Delete(path);
                return;
            }

            CreateFile(path, sql);
        }
        void CreateFile(string path, SqlModel sql)
        {
            try
            {
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine("<connectionStrings> ");
                    sw.WriteLine($"<add name='{_nameConnection}' connectionString='Data Source={sql.Server}; Initial Catalog={sql.Db}; User ID={sql.User}; Password={sql.Password}' ");
                    sw.WriteLine("providerName='System.Data.SqlClient'/> ");
                    sw.WriteLine("</connectionStrings>");

                    //Messa
                    MessageBox.Show("Se ha inicializado los files de conexión a SQL, se cerrará la aplicación. Por favor, reinicie el sistema.");
                    Set();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        void UpdateConnections()
        {
            try
            {
                var sqlModel = new SqlModel()
                {
                    Server = txServer.Text.Trim().ToLower(),
                    User = txUser.Text.Trim().ToLower(),
                    Password = txPassword.Text.Trim(),
                    Db = cbData.Text.Trim()
                };

                string query = string.Empty;
                int count = 0;
                foreach (var x in _model)
                {
                    count++;
                    query = $"UPDATE {x.TableName} " +
                        $"SET {x.SetColumn}='{GetValues(x.Identity)}' " +
                        $"WHERE {x.WhereColumn}='{x.WhereValue}'";

                    SqlContext.ExecuteSqlCommand(query, sqlModel);
                }
                if (count > 0)
                {
                    SetFileConnection(sqlModel);
                    Set();
                    return;
                }

                MessageBox.Show("No ha sido posible escribir los parámetros de conexión, reportar al administrador de la aplicación.");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        string GetValues(SqlIdentity identity)
        {
            string r = string.Empty;
            switch (identity)
            {
                case SqlIdentity.SqlServer:
                    r = txServer.Text.Trim();
                    break;
                case SqlIdentity.SqlBase:
                    r = cbData.Text.Trim();
                    break;
                case SqlIdentity.SqlUser:
                    r = txUser.Text.Trim();
                    break;
                case SqlIdentity.SqlPassword:
                    r = txPassword.Text.Trim();
                    break;
            }
            return r;
        }

        #endregion

        private void btnConnected_Click(object sender, EventArgs e)
        {
            UpdateConnections();
        }
    }


}
