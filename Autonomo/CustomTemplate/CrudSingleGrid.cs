﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autonomo.CustomTemplate
{
    public partial class CrudSingleGrid : Form
    {
        Color _lineColor;
        Color _lineBottom;
        bool _moveForm;
        public CrudSingleGrid()
        {
            InitializeComponent();
            _lineColor = PanelTop.BackColor;
            TopLine.BackColor = _lineColor;
            _lineBottom = PanelBottom.BackColor;
            BottomLine.BackColor = _lineBottom;

            _moveForm = false;
        }

        [Category("Autonomo properties")]
        [Description("Change: True = Move formulary and False: Disabled move formulary.")]
        public bool MoveFormumary
        {
            get { return _moveForm; }
            set { _moveForm = value; }
        }

        [Category("Autonomo properties")]
        public Color GridLineColor {
            get { return _lineColor; }
            set
            {
                _lineColor = value;
                TopLine.BackColor = _lineColor;
            }
        }

        [Category("Autonomo properties")]
        public Color DetailLineColor
        {
            get { return _lineBottom; }
            set
            {
                _lineBottom = value;
                BottomLine.BackColor = _lineBottom;
            }
        }

        private void CrudSingleGrid_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r' || e.KeyChar == (char)Keys.Escape)
                e.Handled = true;
        }

        private void CrudSingleGrid_Load(object sender, EventArgs e)
        {
            txFilter.Focus();
        }

        private void FormTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (_moveForm)
                Autonomo.Class.MoveObject.MoveForm(this);
        }
    }
}
