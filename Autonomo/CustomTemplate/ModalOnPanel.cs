﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Autonomo.CustomControls.HelperControl;

namespace Autonomo.CustomTemplate
{
    public partial class ModalOnPanel : Form
    {
        State StateForm;
        bool _moveForm;
        Color _topColor;
        public ModalOnPanel()
        {
            InitializeComponent();
          
            StateForm = State.Cancel;
            _moveForm = false;
            _topColor = PanelTop.BackColor;
        }

        [Category("Autonomo properties")]
        [Description("Change: True = Move formulary and False: Disabled move formulary.")]
        public BorderStyle BorderNewStyle
        {
            get { return CustomContainer.BorderStyle; }
            set { CustomContainer.BorderStyle = value; }
        }

        [Category("Autonomo properties")]
        [Description("Change: True = Move formulary and False: Disabled move formulary.")]
        public bool MoveFormumary
        {
            get { return _moveForm; }
            set
            {
                _moveForm = value;
                if (value)
                    PanelTop.Cursor = Cursors.Hand;
                else
                    PanelTop.Cursor = Cursors.Default;
            }
        }

        [Category("Autonomo properties")]
        public Color TopBackColor
        {
            get { return _topColor; }
            set
            {
                _topColor = value;
                PanelTop.BackColor = _topColor;
            }
        }

        [Category("Autonomo properties")]
        public void ThemeStyle(Theme theme)
        {
            switch (theme)
            {
                case Theme.White:
                    break;
                case Theme.Dark:
                    break;
                case Theme.BlueDark:
                    break;
                case Theme.Green:
                    break;
                default:
                    break;
            }
        }

        [Category("Autonomo properties")]
        public void ConfigButton(Color buttonBackColor, Color buttonForeColor, int x, int y)
        {
            btnSave.BackColor = buttonBackColor;
            btnSave.ForeColor = buttonForeColor;
            Class.RoundObject.RoundButton(btnSave, x, y);
        }

        public void Set()
        {
            this.Tag = "Get";
            StateForm = State.Success;
            this.Close();
        }

        private void Cancel()
        {
            this.Tag = "None";
            StateForm = State.Cancel;
            this.Close();
        }

        /// <summary>
        /// Estado del formulario, nos permite validar si se ha seleccionado los valores de la grilla 
        /// o si se ha cerrado el formulario sin seleccionar la data.
        /// </summary>
        [Category("Autonomo properties")]
        public bool StateFormulary
        {
            get
            {
                return StateForm == State.Success ? true : false;
            }
        }

        private void FormCloseControl_Click(object sender, EventArgs e)
        {
            Cancel();
        }

        private void ModalOnPanel_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r' || e.KeyChar == (char)Keys.Escape)
                e.Handled = true;
        }

        private void ModalOnPanel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape) { Cancel(); }
        }

        private void label1_MouseDown(object sender, MouseEventArgs e)
        {
            if (_moveForm)
                Autonomo.Class.MoveObject.MoveForm(this);
        }

        private void ModalOnPanel_Load(object sender, EventArgs e)
        {
            //  (new Class.DropShadow()).ApplyShadows(this);
        }
    }
}
