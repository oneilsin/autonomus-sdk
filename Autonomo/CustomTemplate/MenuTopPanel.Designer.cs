﻿
namespace Autonomo.CustomTemplate
{
    partial class MenuTopPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuTopPanel));
            this.TopPanel = new System.Windows.Forms.Panel();
            this.t = new System.Windows.Forms.TextBox();
            this.Maximizar = new System.Windows.Forms.Button();
            this.sesion = new System.Windows.Forms.Label();
            this.Description = new System.Windows.Forms.Label();
            this.Minimizar = new System.Windows.Forms.Button();
            this.Company = new System.Windows.Forms.Label();
            this.Logo = new System.Windows.Forms.PictureBox();
            this.Salir = new System.Windows.Forms.Button();
            this.BarPanel = new System.Windows.Forms.Panel();
            this._space = new System.Windows.Forms.Label();
            this.CustomContainer = new System.Windows.Forms.Panel();
            this.abajo = new System.Windows.Forms.Label();
            this.tmrDateTime = new System.Windows.Forms.Timer(this.components);
            this._abajo = new System.Windows.Forms.Panel();
            this.time = new System.Windows.Forms.Label();
            this.MainPanel = new System.Windows.Forms.Panel();
            this.derecha = new System.Windows.Forms.Label();
            this.izquierda = new System.Windows.Forms.Label();
            this.arriba = new System.Windows.Forms.Label();
            this._profile = new System.Windows.Forms.Panel();
            this.help = new System.Windows.Forms.Label();
            this.about = new System.Windows.Forms.Label();
            this.profile = new System.Windows.Forms.Label();
            this.logOut = new System.Windows.Forms.Label();
            this.userName = new System.Windows.Forms.Label();
            this.picture = new System.Windows.Forms.PictureBox();
            this.PanelTop = new System.Windows.Forms.Panel();
            this.role = new System.Windows.Forms.Label();
            this.PopUpClose = new System.Windows.Forms.Label();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).BeginInit();
            this.BarPanel.SuspendLayout();
            this._abajo.SuspendLayout();
            this.MainPanel.SuspendLayout();
            this._profile.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picture)).BeginInit();
            this.PanelTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // TopPanel
            // 
            this.TopPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            this.TopPanel.Controls.Add(this.t);
            this.TopPanel.Controls.Add(this.Maximizar);
            this.TopPanel.Controls.Add(this.sesion);
            this.TopPanel.Controls.Add(this.Description);
            this.TopPanel.Controls.Add(this.Minimizar);
            this.TopPanel.Controls.Add(this.Company);
            this.TopPanel.Controls.Add(this.Logo);
            this.TopPanel.Controls.Add(this.Salir);
            this.TopPanel.Controls.Add(this.BarPanel);
            this.TopPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopPanel.Location = new System.Drawing.Point(0, 0);
            this.TopPanel.Name = "TopPanel";
            this.TopPanel.Size = new System.Drawing.Size(819, 113);
            this.TopPanel.TabIndex = 0;
            this.TopPanel.DoubleClick += new System.EventHandler(this.TopPanel_DoubleClick);
            this.TopPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TopPanel_MouseDown);
            // 
            // t
            // 
            this.t.BackColor = System.Drawing.Color.White;
            this.t.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.t.Dock = System.Windows.Forms.DockStyle.Right;
            this.t.Location = new System.Drawing.Point(818, 0);
            this.t.MaxLength = 20;
            this.t.Name = "t";
            this.t.Size = new System.Drawing.Size(1, 13);
            this.t.TabIndex = 0;
            // 
            // Maximizar
            // 
            this.Maximizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Maximizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Maximizar.FlatAppearance.BorderSize = 0;
            this.Maximizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Maximizar.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Bold);
            this.Maximizar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Maximizar.Location = new System.Drawing.Point(731, 4);
            this.Maximizar.Name = "Maximizar";
            this.Maximizar.Size = new System.Drawing.Size(38, 32);
            this.Maximizar.TabIndex = 1;
            this.Maximizar.Text = "▓";
            this.Maximizar.UseVisualStyleBackColor = true;
            this.Maximizar.Click += new System.EventHandler(this.Maximizar_Click);
            // 
            // sesion
            // 
            this.sesion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sesion.Cursor = System.Windows.Forms.Cursors.Hand;
            this.sesion.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sesion.Image = ((System.Drawing.Image)(resources.GetObject("sesion.Image")));
            this.sesion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.sesion.Location = new System.Drawing.Point(421, 6);
            this.sesion.Name = "sesion";
            this.sesion.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.sesion.Size = new System.Drawing.Size(260, 28);
            this.sesion.TabIndex = 0;
            this.sesion.Text = "      Welcome";
            this.sesion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.sesion.Click += new System.EventHandler(this.sesion_Click);
            // 
            // Description
            // 
            this.Description.AutoSize = true;
            this.Description.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Description.Location = new System.Drawing.Point(146, 34);
            this.Description.Name = "Description";
            this.Description.Size = new System.Drawing.Size(161, 14);
            this.Description.TabIndex = 0;
            this.Description.Text = "Códigos, Librerías y más";
            this.Description.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TopPanel_MouseDown);
            // 
            // Minimizar
            // 
            this.Minimizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Minimizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Minimizar.FlatAppearance.BorderSize = 0;
            this.Minimizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Minimizar.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Bold);
            this.Minimizar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Minimizar.Location = new System.Drawing.Point(687, 4);
            this.Minimizar.Name = "Minimizar";
            this.Minimizar.Size = new System.Drawing.Size(38, 32);
            this.Minimizar.TabIndex = 1;
            this.Minimizar.Text = "─";
            this.Minimizar.UseVisualStyleBackColor = true;
            this.Minimizar.Click += new System.EventHandler(this.Minimizar_Click);
            // 
            // Company
            // 
            this.Company.AutoSize = true;
            this.Company.Font = new System.Drawing.Font("Verdana", 13F, System.Drawing.FontStyle.Bold);
            this.Company.Location = new System.Drawing.Point(146, 9);
            this.Company.Name = "Company";
            this.Company.Size = new System.Drawing.Size(173, 22);
            this.Company.TabIndex = 0;
            this.Company.Text = "Autonomus SDK";
            this.Company.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TopPanel_MouseDown);
            // 
            // Logo
            // 
            this.Logo.Image = ((System.Drawing.Image)(resources.GetObject("Logo.Image")));
            this.Logo.Location = new System.Drawing.Point(3, 3);
            this.Logo.Name = "Logo";
            this.Logo.Size = new System.Drawing.Size(121, 56);
            this.Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Logo.TabIndex = 10;
            this.Logo.TabStop = false;
            this.Logo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TopPanel_MouseDown);
            // 
            // Salir
            // 
            this.Salir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Salir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Salir.FlatAppearance.BorderSize = 0;
            this.Salir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.Salir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Salir.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Bold);
            this.Salir.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Salir.Location = new System.Drawing.Point(775, 4);
            this.Salir.Name = "Salir";
            this.Salir.Size = new System.Drawing.Size(38, 32);
            this.Salir.TabIndex = 1;
            this.Salir.Text = "X";
            this.Salir.UseVisualStyleBackColor = true;
            this.Salir.Click += new System.EventHandler(this.Salir_Click);
            // 
            // BarPanel
            // 
            this.BarPanel.Controls.Add(this._space);
            this.BarPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BarPanel.Location = new System.Drawing.Point(0, 64);
            this.BarPanel.Name = "BarPanel";
            this.BarPanel.Size = new System.Drawing.Size(819, 49);
            this.BarPanel.TabIndex = 1;
            // 
            // _space
            // 
            this._space.Dock = System.Windows.Forms.DockStyle.Left;
            this._space.Location = new System.Drawing.Point(0, 0);
            this._space.Name = "_space";
            this._space.Size = new System.Drawing.Size(27, 49);
            this._space.TabIndex = 0;
            // 
            // CustomContainer
            // 
            this.CustomContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CustomContainer.Location = new System.Drawing.Point(10, 123);
            this.CustomContainer.Name = "CustomContainer";
            this.CustomContainer.Size = new System.Drawing.Size(799, 575);
            this.CustomContainer.TabIndex = 1;
            // 
            // abajo
            // 
            this.abajo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            this.abajo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.abajo.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.abajo.Location = new System.Drawing.Point(0, 0);
            this.abajo.Name = "abajo";
            this.abajo.Size = new System.Drawing.Size(799, 23);
            this.abajo.TabIndex = 6;
            this.abajo.Text = "Módulo {0} : Conexión {1} - {2} : Usuario {3} ";
            this.abajo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tmrDateTime
            // 
            this.tmrDateTime.Enabled = true;
            this.tmrDateTime.Interval = 10;
            this.tmrDateTime.Tick += new System.EventHandler(this.tmrDateTime_Tick);
            // 
            // _abajo
            // 
            this._abajo.Controls.Add(this.time);
            this._abajo.Controls.Add(this.abajo);
            this._abajo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._abajo.Location = new System.Drawing.Point(10, 698);
            this._abajo.Name = "_abajo";
            this._abajo.Size = new System.Drawing.Size(799, 23);
            this._abajo.TabIndex = 7;
            // 
            // time
            // 
            this.time.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            this.time.Dock = System.Windows.Forms.DockStyle.Right;
            this.time.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.time.Location = new System.Drawing.Point(434, 0);
            this.time.Name = "time";
            this.time.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.time.Size = new System.Drawing.Size(365, 23);
            this.time.TabIndex = 7;
            this.time.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // MainPanel
            // 
            this.MainPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MainPanel.Controls.Add(this.CustomContainer);
            this.MainPanel.Controls.Add(this._abajo);
            this.MainPanel.Controls.Add(this.derecha);
            this.MainPanel.Controls.Add(this.izquierda);
            this.MainPanel.Controls.Add(this.arriba);
            this.MainPanel.Controls.Add(this.TopPanel);
            this.MainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainPanel.Location = new System.Drawing.Point(0, 0);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(821, 723);
            this.MainPanel.TabIndex = 0;
            // 
            // derecha
            // 
            this.derecha.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            this.derecha.Dock = System.Windows.Forms.DockStyle.Right;
            this.derecha.Location = new System.Drawing.Point(809, 123);
            this.derecha.Name = "derecha";
            this.derecha.Size = new System.Drawing.Size(10, 598);
            this.derecha.TabIndex = 7;
            // 
            // izquierda
            // 
            this.izquierda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            this.izquierda.Dock = System.Windows.Forms.DockStyle.Left;
            this.izquierda.Location = new System.Drawing.Point(0, 123);
            this.izquierda.Name = "izquierda";
            this.izquierda.Size = new System.Drawing.Size(10, 598);
            this.izquierda.TabIndex = 6;
            // 
            // arriba
            // 
            this.arriba.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            this.arriba.Dock = System.Windows.Forms.DockStyle.Top;
            this.arriba.Location = new System.Drawing.Point(0, 113);
            this.arriba.Name = "arriba";
            this.arriba.Size = new System.Drawing.Size(819, 10);
            this.arriba.TabIndex = 5;
            // 
            // _profile
            // 
            this._profile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._profile.Controls.Add(this.help);
            this._profile.Controls.Add(this.about);
            this._profile.Controls.Add(this.profile);
            this._profile.Controls.Add(this.logOut);
            this._profile.Controls.Add(this.userName);
            this._profile.Controls.Add(this.picture);
            this._profile.Controls.Add(this.PanelTop);
            this._profile.Location = new System.Drawing.Point(582, 42);
            this._profile.Name = "_profile";
            this._profile.Size = new System.Drawing.Size(230, 301);
            this._profile.TabIndex = 1;
            this._profile.Visible = false;
            // 
            // help
            // 
            this.help.Cursor = System.Windows.Forms.Cursors.Hand;
            this.help.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.help.ForeColor = System.Drawing.Color.DimGray;
            this.help.Image = ((System.Drawing.Image)(resources.GetObject("help.Image")));
            this.help.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.help.Location = new System.Drawing.Point(5, 184);
            this.help.Name = "help";
            this.help.Size = new System.Drawing.Size(220, 33);
            this.help.TabIndex = 15;
            this.help.Text = "        Do you need help?";
            this.help.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.help.MouseEnter += new System.EventHandler(this.help_MouseEnter);
            this.help.MouseLeave += new System.EventHandler(this.help_MouseLeave);
            // 
            // about
            // 
            this.about.Cursor = System.Windows.Forms.Cursors.Hand;
            this.about.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.about.ForeColor = System.Drawing.Color.DimGray;
            this.about.Image = ((System.Drawing.Image)(resources.GetObject("about.Image")));
            this.about.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.about.Location = new System.Drawing.Point(5, 217);
            this.about.Name = "about";
            this.about.Size = new System.Drawing.Size(220, 33);
            this.about.TabIndex = 14;
            this.about.Text = "        About Application";
            this.about.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.about.MouseEnter += new System.EventHandler(this.about_MouseEnter);
            this.about.MouseLeave += new System.EventHandler(this.about_MouseLeave);
            // 
            // profile
            // 
            this.profile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.profile.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.profile.ForeColor = System.Drawing.Color.DimGray;
            this.profile.Image = ((System.Drawing.Image)(resources.GetObject("profile.Image")));
            this.profile.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.profile.Location = new System.Drawing.Point(5, 151);
            this.profile.Name = "profile";
            this.profile.Size = new System.Drawing.Size(220, 33);
            this.profile.TabIndex = 14;
            this.profile.Text = "        Configure your Profile";
            this.profile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.profile.MouseEnter += new System.EventHandler(this.profile_MouseEnter);
            this.profile.MouseLeave += new System.EventHandler(this.profile_MouseLeave);
            // 
            // logOut
            // 
            this.logOut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.logOut.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logOut.ForeColor = System.Drawing.Color.DimGray;
            this.logOut.Image = ((System.Drawing.Image)(resources.GetObject("logOut.Image")));
            this.logOut.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.logOut.Location = new System.Drawing.Point(5, 250);
            this.logOut.Name = "logOut";
            this.logOut.Size = new System.Drawing.Size(220, 33);
            this.logOut.TabIndex = 14;
            this.logOut.Text = "        Log Out";
            this.logOut.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.logOut.MouseEnter += new System.EventHandler(this.logOut_MouseEnter);
            this.logOut.MouseLeave += new System.EventHandler(this.logOut_MouseLeave);
            // 
            // userName
            // 
            this.userName.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold);
            this.userName.ForeColor = System.Drawing.Color.DimGray;
            this.userName.Location = new System.Drawing.Point(108, 50);
            this.userName.Name = "userName";
            this.userName.Size = new System.Drawing.Size(117, 90);
            this.userName.TabIndex = 12;
            this.userName.Text = "Juan manuel del Prado y de la Costanza";
            this.userName.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // picture
            // 
            this.picture.Image = global::Autonomo.Properties.Resources.Photo;
            this.picture.Location = new System.Drawing.Point(3, 50);
            this.picture.Name = "picture";
            this.picture.Size = new System.Drawing.Size(102, 90);
            this.picture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picture.TabIndex = 11;
            this.picture.TabStop = false;
            // 
            // PanelTop
            // 
            this.PanelTop.BackColor = System.Drawing.Color.Transparent;
            this.PanelTop.Controls.Add(this.role);
            this.PanelTop.Controls.Add(this.PopUpClose);
            this.PanelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelTop.Location = new System.Drawing.Point(0, 0);
            this.PanelTop.Name = "PanelTop";
            this.PanelTop.Size = new System.Drawing.Size(228, 44);
            this.PanelTop.TabIndex = 1;
            // 
            // role
            // 
            this.role.AutoSize = true;
            this.role.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold);
            this.role.Location = new System.Drawing.Point(10, 13);
            this.role.Name = "role";
            this.role.Size = new System.Drawing.Size(77, 17);
            this.role.TabIndex = 3;
            this.role.Text = "Role {0}";
            // 
            // PopUpClose
            // 
            this.PopUpClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PopUpClose.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PopUpClose.Location = new System.Drawing.Point(190, 3);
            this.PopUpClose.Name = "PopUpClose";
            this.PopUpClose.Size = new System.Drawing.Size(35, 31);
            this.PopUpClose.TabIndex = 2;
            this.PopUpClose.Text = "X";
            this.PopUpClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.PopUpClose.Click += new System.EventHandler(this.PopUpClose_Click);
            // 
            // MenuTopPanel
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(821, 723);
            this.Controls.Add(this._profile);
            this.Controls.Add(this.MainPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "MenuTopPanel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MenuTopPanel";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MenuTopPanel_FormClosing);
            this.Load += new System.EventHandler(this.MenuTopPanel_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MenuTopPanel_KeyPress);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).EndInit();
            this.BarPanel.ResumeLayout(false);
            this._abajo.ResumeLayout(false);
            this.MainPanel.ResumeLayout(false);
            this._profile.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picture)).EndInit();
            this.PanelTop.ResumeLayout(false);
            this.PanelTop.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel TopPanel;
        private System.Windows.Forms.Label abajo;
        private System.Windows.Forms.Button Minimizar;
        private System.Windows.Forms.Button Salir;
        public System.Windows.Forms.PictureBox Logo;
        public System.Windows.Forms.Label Description;
        public System.Windows.Forms.Label Company;
        public System.Windows.Forms.Label _space;
        public System.Windows.Forms.Panel CustomContainer;
        private System.Windows.Forms.Panel _abajo;
        private System.Windows.Forms.Label time;
        private System.Windows.Forms.Timer tmrDateTime;
        private System.Windows.Forms.Label sesion;
        private System.Windows.Forms.Button Maximizar;
        private System.Windows.Forms.TextBox t;
        public System.Windows.Forms.Panel MainPanel;
        private System.Windows.Forms.Label derecha;
        private System.Windows.Forms.Label izquierda;
        private System.Windows.Forms.Label arriba;
        public System.Windows.Forms.Panel BarPanel;
        private System.Windows.Forms.Panel _profile;
        public System.Windows.Forms.Panel PanelTop;
        private System.Windows.Forms.Label help;
        private System.Windows.Forms.Label about;
        private System.Windows.Forms.Label profile;
        private System.Windows.Forms.Label logOut;
        private System.Windows.Forms.Label userName;
        private System.Windows.Forms.Label role;
        private System.Windows.Forms.PictureBox picture;
        private System.Windows.Forms.Label PopUpClose;
    }
}