﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Autonomo.CustomTemplate.Emuns;

namespace Autonomo.CustomTemplate
{
    public partial class MenuSlideBar : Form
    {
        Display display;
        Theme theme;
        public MenuSlideBar()
        {
            InitializeComponent();
            display = Display.Full;
            theme = Theme.Light;
        }

        private void Logo_Click(object sender, EventArgs e)
        {
            CustomSlide();
        }
        private void MenuSlideBar_Load(object sender, EventArgs e)
        {
            ManageTheme();
        }

        #region Controllers
        void CustomSlide()
        {
            switch (display)
            {
                case Display.None:
                    break;
                case Display.Short:
                    {
                        display = Display.Full;
                        Logo.Location = new Point(84, 3);
                        Logo.Size = new Size(82, 50);
                        hiddenTitles(true, 236);
                        break;
                    }
                case Display.Full:
                    {
                        display = Display.Short;
                        Logo.Location = new Point(2, 2);
                        Logo.Size = new Size(48, 50);
                        hiddenTitles(false, 52);
                        break;
                    }
            }
        }

        void hiddenTitles(bool value, int width)
        {
            Title.Visible = value;
            SubTitle.Visible = value;
            _title.Visible = !value;
            DivLeftSlide.Width = width;
        }

        void ManageTheme()
        {
            switch (theme)
            {
                case Theme.Light:
                    LightColor();
                    break;
                case Theme.Dark:
                    DarkColor();
                    break;
                case Theme.BlueDark:
                    break;
            }
        }

        void LightColor()
        {
            Title.ForeColor = Color.Black;
            SubTitle.ForeColor = Color.Black;          
            _modules.ForeColor = Color.Silver;
            _usuario.ForeColor = Color.FromArgb(64, 64, 64);
            _rol.ForeColor = Color.FromArgb(64, 64, 64);
            _version.ForeColor = Color.Gray;
            separRight.BackColor = DivSlideContainer.BackColor;
        }
        void DarkColor()
        {
            Title.ForeColor = Color.WhiteSmoke;
            SubTitle.ForeColor = Color.WhiteSmoke;
            _modules.ForeColor = Color.White;
            _usuario.ForeColor = Color.WhiteSmoke;
            _rol.ForeColor = Color.WhiteSmoke;
            _version.ForeColor = Color.White;
            separRight.BackColor = DivSlideContainer.BackColor;
        }
        #endregion


        #region Attributes
        [Category("Autonomo properties")]
        public Theme Themes
        {
            get { return theme; }
            set { theme = value; }
        }

        [Category("Autonomo properties")]
        public Color SeparatorColor
        {
            get { return separatorSlide.BackColor; }
            set { separatorSlide.BackColor = value; }
        }
        #endregion
        private void cmdExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmdMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void MenuSlideBar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r' || e.KeyChar == (char)Keys.Escape)
                e.Handled = true;
        }

        private void MenuSlideBar_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                //string result = Autonomus.Class.Message.MessageBox(3,
                //"¡Hasta pronto!. No hay procesos pentientes, puede salir de la Aplicación sin problemas." + Environment.NewLine + Environment.NewLine +
                //"¿Estás seguro de continuar?");
                if ("Yes" == "Yes")
                { Application.Exit(); }
                else
                { e.Cancel.Equals(true); }
            }
        }

        private void _title_Click(object sender, EventArgs e)
        {
            CustomSlide();
        }
    }
    public class Emuns
    {
        public enum Display { None, Short, Full }
        public enum Theme { Light, Dark, BlueDark }
    }
}
