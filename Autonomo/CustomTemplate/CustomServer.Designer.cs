﻿namespace Autonomo.CustomTemplate
{
    partial class CustomServer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomServer));
            this.CustomContainer = new System.Windows.Forms.Panel();
            this.PanelControls = new System.Windows.Forms.Panel();
            this.lbMessage = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txPassword = new Autonomo.CustomControls.FlatTextBox();
            this.PanelBottom = new System.Windows.Forms.Panel();
            this.PanelTop = new System.Windows.Forms.Panel();
            this.PanelTitle = new System.Windows.Forms.Panel();
            this.FormTitle = new System.Windows.Forms.Label();
            this.FormCloseControl = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbData = new Autonomo.CustomControls.FlatComboBox();
            this.txUser = new Autonomo.CustomControls.FlatTextBox();
            this.txServer = new Autonomo.CustomControls.FlatTextBox();
            this.btnConnected = new System.Windows.Forms.Button();
            this.CustomContainer.SuspendLayout();
            this.PanelControls.SuspendLayout();
            this.PanelBottom.SuspendLayout();
            this.PanelTop.SuspendLayout();
            this.PanelTitle.SuspendLayout();
            this.SuspendLayout();
            // 
            // CustomContainer
            // 
            this.CustomContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CustomContainer.Controls.Add(this.PanelControls);
            this.CustomContainer.Controls.Add(this.PanelBottom);
            this.CustomContainer.Controls.Add(this.PanelTop);
            this.CustomContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CustomContainer.Location = new System.Drawing.Point(0, 0);
            this.CustomContainer.Name = "CustomContainer";
            this.CustomContainer.Size = new System.Drawing.Size(292, 397);
            this.CustomContainer.TabIndex = 2;
            // 
            // PanelControls
            // 
            this.PanelControls.AutoScroll = true;
            this.PanelControls.Controls.Add(this.lbMessage);
            this.PanelControls.Controls.Add(this.label2);
            this.PanelControls.Controls.Add(this.cbData);
            this.PanelControls.Controls.Add(this.txPassword);
            this.PanelControls.Controls.Add(this.txUser);
            this.PanelControls.Controls.Add(this.txServer);
            this.PanelControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelControls.Location = new System.Drawing.Point(0, 40);
            this.PanelControls.Name = "PanelControls";
            this.PanelControls.Size = new System.Drawing.Size(290, 296);
            this.PanelControls.TabIndex = 0;
            // 
            // lbMessage
            // 
            this.lbMessage.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMessage.ForeColor = System.Drawing.Color.White;
            this.lbMessage.Location = new System.Drawing.Point(9, 189);
            this.lbMessage.Name = "lbMessage";
            this.lbMessage.Size = new System.Drawing.Size(275, 23);
            this.lbMessage.TabIndex = 6;
            this.lbMessage.Text = "--";
            this.lbMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Verdana", 8.75F);
            this.label2.ForeColor = System.Drawing.Color.DimGray;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(290, 49);
            this.label2.TabIndex = 5;
            this.label2.Text = "Importante: La ruta para el servidor acepta formato IP y Texto, ejemplo “190.168." +
    "1.201” o “SERVER-SQL-TEST”.";
            this.label2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FormTitle_MouseDown);
            // 
            // txPassword
            // 
            this.txPassword.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txPassword.BackColor = System.Drawing.Color.White;
            this.txPassword.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txPassword.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txPassword.ColorLine = System.Drawing.Color.Gray;
            this.txPassword.ColorText = System.Drawing.SystemColors.WindowText;
            this.txPassword.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txPassword.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txPassword.Error = "";
            this.txPassword.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txPassword.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txPassword.FormatLogin = false;
            this.txPassword.ImageIcon = null;
            this.txPassword.Info = "Key Enter to Connection";
            this.txPassword.Location = new System.Drawing.Point(134, 124);
            this.txPassword.MaterialStyle = true;
            this.txPassword.MaxLength = 32767;
            this.txPassword.MultiLineText = false;
            this.txPassword.Name = "txPassword";
            this.txPassword.PasswordChar = '•';
            this.txPassword.Placeholder = "Contraseña SQL";
            this.txPassword.ReadOnly = false;
            this.txPassword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txPassword.Size = new System.Drawing.Size(148, 58);
            this.txPassword.SizeLine = 2;
            this.txPassword.TabIndex = 2;
            this.txPassword.Title = "Contraseña SQL";
            this.txPassword.VisibleIcon = false;
            this.txPassword.VisibleTitle = false;
            this.txPassword.KeyDown += new System.EventHandler<System.Windows.Forms.KeyEventArgs>(this.txPassword_KeyDown);
            this.txPassword.Leave += new System.EventHandler(this.txPassword_Leave);
            // 
            // PanelBottom
            // 
            this.PanelBottom.BackColor = System.Drawing.Color.White;
            this.PanelBottom.Controls.Add(this.btnConnected);
            this.PanelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelBottom.Location = new System.Drawing.Point(0, 336);
            this.PanelBottom.Name = "PanelBottom";
            this.PanelBottom.Size = new System.Drawing.Size(290, 59);
            this.PanelBottom.TabIndex = 2;
            // 
            // PanelTop
            // 
            this.PanelTop.BackColor = System.Drawing.Color.White;
            this.PanelTop.Controls.Add(this.PanelTitle);
            this.PanelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelTop.Location = new System.Drawing.Point(0, 0);
            this.PanelTop.Name = "PanelTop";
            this.PanelTop.Size = new System.Drawing.Size(290, 40);
            this.PanelTop.TabIndex = 0;
            // 
            // PanelTitle
            // 
            this.PanelTitle.Controls.Add(this.FormTitle);
            this.PanelTitle.Controls.Add(this.FormCloseControl);
            this.PanelTitle.Controls.Add(this.label1);
            this.PanelTitle.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelTitle.Location = new System.Drawing.Point(0, 2);
            this.PanelTitle.Name = "PanelTitle";
            this.PanelTitle.Size = new System.Drawing.Size(290, 38);
            this.PanelTitle.TabIndex = 0;
            // 
            // FormTitle
            // 
            this.FormTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FormTitle.Font = new System.Drawing.Font("Verdana", 14F);
            this.FormTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.FormTitle.Location = new System.Drawing.Point(4, 0);
            this.FormTitle.Name = "FormTitle";
            this.FormTitle.Size = new System.Drawing.Size(251, 38);
            this.FormTitle.TabIndex = 0;
            this.FormTitle.Text = "Manage SQL Connection";
            this.FormTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.FormTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FormTitle_MouseDown);
            // 
            // FormCloseControl
            // 
            this.FormCloseControl.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FormCloseControl.Dock = System.Windows.Forms.DockStyle.Right;
            this.FormCloseControl.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormCloseControl.Location = new System.Drawing.Point(255, 0);
            this.FormCloseControl.Name = "FormCloseControl";
            this.FormCloseControl.Size = new System.Drawing.Size(35, 38);
            this.FormCloseControl.TabIndex = 1;
            this.FormCloseControl.Text = "X";
            this.FormCloseControl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.FormCloseControl.Click += new System.EventHandler(this.FormCloseControl_Click);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(4, 38);
            this.label1.TabIndex = 0;
            // 
            // cbData
            // 
            this.cbData.BackColor = System.Drawing.Color.White;
            this.cbData.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.cbData.ColorLine = System.Drawing.Color.Gray;
            this.cbData.ColorText = System.Drawing.SystemColors.WindowText;
            this.cbData.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbData.DisplayMember = "";
            this.cbData.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.cbData.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbData.Error = "";
            this.cbData.FontText = new System.Drawing.Font("Verdana", 10F);
            this.cbData.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.cbData.ImageIcon = ((System.Drawing.Image)(resources.GetObject("cbData.ImageIcon")));
            this.cbData.Info = "Establecer base de datos de Aplicación";
            this.cbData.Location = new System.Drawing.Point(10, 220);
            this.cbData.MaterialStyle = true;
            this.cbData.Name = "cbData";
            this.cbData.Placeholder = "-Select- Base de datos SQL";
            this.cbData.SelectedIndex = -1;
            this.cbData.Size = new System.Drawing.Size(272, 58);
            this.cbData.SizeLine = 2;
            this.cbData.TabIndex = 3;
            this.cbData.Title = "-Select- Base de datos SQL";
            this.cbData.ValueMember = "";
            this.cbData.VisibleIcon = true;
            this.cbData.VisibleTitle = false;
            // 
            // txUser
            // 
            this.txUser.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txUser.BackColor = System.Drawing.Color.White;
            this.txUser.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txUser.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txUser.ColorLine = System.Drawing.Color.Gray;
            this.txUser.ColorText = System.Drawing.SystemColors.WindowText;
            this.txUser.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txUser.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txUser.Error = "";
            this.txUser.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txUser.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txUser.FormatLogin = false;
            this.txUser.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txUser.ImageIcon")));
            this.txUser.Info = "defaults";
            this.txUser.Location = new System.Drawing.Point(10, 124);
            this.txUser.MaterialStyle = true;
            this.txUser.MaxLength = 32767;
            this.txUser.MultiLineText = false;
            this.txUser.Name = "txUser";
            this.txUser.PasswordChar = '\0';
            this.txUser.Placeholder = "Usuario SQL";
            this.txUser.ReadOnly = false;
            this.txUser.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txUser.Size = new System.Drawing.Size(118, 58);
            this.txUser.SizeLine = 2;
            this.txUser.TabIndex = 1;
            this.txUser.Title = "Usuario SQL";
            this.txUser.VisibleIcon = true;
            this.txUser.VisibleTitle = false;
            // 
            // txServer
            // 
            this.txServer.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txServer.BackColor = System.Drawing.Color.White;
            this.txServer.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txServer.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txServer.ColorLine = System.Drawing.Color.Gray;
            this.txServer.ColorText = System.Drawing.SystemColors.WindowText;
            this.txServer.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txServer.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txServer.Error = "";
            this.txServer.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txServer.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txServer.FormatLogin = false;
            this.txServer.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txServer.ImageIcon")));
            this.txServer.Info = "Server name or IP address";
            this.txServer.Location = new System.Drawing.Point(10, 60);
            this.txServer.MaterialStyle = true;
            this.txServer.MaxLength = 32767;
            this.txServer.MultiLineText = false;
            this.txServer.Name = "txServer";
            this.txServer.PasswordChar = '\0';
            this.txServer.Placeholder = "-Input- Ruta del servidor";
            this.txServer.ReadOnly = false;
            this.txServer.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txServer.Size = new System.Drawing.Size(272, 58);
            this.txServer.SizeLine = 2;
            this.txServer.TabIndex = 0;
            this.txServer.Title = "-Input- Ruta del servidor";
            this.txServer.VisibleIcon = true;
            this.txServer.VisibleTitle = false;
            // 
            // btnConnected
            // 
            this.btnConnected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(100)))), ((int)(((byte)(184)))));
            this.btnConnected.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnConnected.FlatAppearance.BorderSize = 0;
            this.btnConnected.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConnected.Font = new System.Drawing.Font("Verdana", 10F);
            this.btnConnected.ForeColor = System.Drawing.Color.White;
            this.btnConnected.Image = ((System.Drawing.Image)(resources.GetObject("btnConnected.Image")));
            this.btnConnected.Location = new System.Drawing.Point(6, 4);
            this.btnConnected.Name = "btnConnected";
            this.btnConnected.Size = new System.Drawing.Size(279, 38);
            this.btnConnected.TabIndex = 2;
            this.btnConnected.Text = "Conexión a base de datos Sql";
            this.btnConnected.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnConnected.UseVisualStyleBackColor = false;
            this.btnConnected.Click += new System.EventHandler(this.btnConnected_Click);
            // 
            // CustomServer
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(292, 397);
            this.Controls.Add(this.CustomContainer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "CustomServer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CustomServer";
            this.Load += new System.EventHandler(this.CustomServer_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CustomServer_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CustomServer_KeyPress);
            this.CustomContainer.ResumeLayout(false);
            this.PanelControls.ResumeLayout(false);
            this.PanelBottom.ResumeLayout(false);
            this.PanelTop.ResumeLayout(false);
            this.PanelTitle.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel CustomContainer;
        public System.Windows.Forms.Panel PanelControls;
        private System.Windows.Forms.Panel PanelBottom;
        public System.Windows.Forms.Button btnConnected;
        public System.Windows.Forms.Panel PanelTop;
        private System.Windows.Forms.Panel PanelTitle;
        public System.Windows.Forms.Label FormTitle;
        public System.Windows.Forms.Label FormCloseControl;
        public System.Windows.Forms.Label label1;
        private CustomControls.FlatTextBox txServer;
        private System.Windows.Forms.Label label2;
        private CustomControls.FlatComboBox cbData;
        private CustomControls.FlatTextBox txUser;
        private System.Windows.Forms.Label lbMessage;
        private CustomControls.FlatTextBox txPassword;
    }
}