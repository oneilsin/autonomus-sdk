﻿namespace Autonomo.CustomTemplate
{
    partial class MenuSlideBar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuSlideBar));
            this.DivTopMenu = new System.Windows.Forms.Panel();
            this._description = new System.Windows.Forms.Label();
            this.divControl = new System.Windows.Forms.Panel();
            this.cmdExit = new System.Windows.Forms.Label();
            this.cmdMinimize = new System.Windows.Forms.Label();
            this._title = new System.Windows.Forms.Label();
            this.DivTemplate = new System.Windows.Forms.Panel();
            this.DivContainer = new System.Windows.Forms.Panel();
            this._right = new System.Windows.Forms.Label();
            this._bottom = new System.Windows.Forms.Label();
            this._left = new System.Windows.Forms.Label();
            this._top = new System.Windows.Forms.Label();
            this.DivLeftSlide = new System.Windows.Forms.Panel();
            this.separatorSlide = new System.Windows.Forms.Label();
            this._modules = new System.Windows.Forms.Label();
            this.DivTopSlideLogo = new System.Windows.Forms.Panel();
            this.SubTitle = new System.Windows.Forms.Label();
            this.Title = new System.Windows.Forms.Label();
            this.DivBottomSlideUser = new System.Windows.Forms.Panel();
            this._version = new System.Windows.Forms.Label();
            this._usuario = new System.Windows.Forms.Label();
            this._rol = new System.Windows.Forms.Label();
            this.Logo = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.DivSlideContainer = new System.Windows.Forms.Panel();
            this.separRight = new System.Windows.Forms.Label();
            this.DivTopMenu.SuspendLayout();
            this.divControl.SuspendLayout();
            this.DivTemplate.SuspendLayout();
            this.DivLeftSlide.SuspendLayout();
            this.DivTopSlideLogo.SuspendLayout();
            this.DivBottomSlideUser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // DivTopMenu
            // 
            this.DivTopMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.DivTopMenu.Controls.Add(this._description);
            this.DivTopMenu.Controls.Add(this.divControl);
            this.DivTopMenu.Controls.Add(this._title);
            this.DivTopMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.DivTopMenu.Location = new System.Drawing.Point(236, 0);
            this.DivTopMenu.Name = "DivTopMenu";
            this.DivTopMenu.Size = new System.Drawing.Size(762, 36);
            this.DivTopMenu.TabIndex = 0;
            // 
            // _description
            // 
            this._description.Dock = System.Windows.Forms.DockStyle.Fill;
            this._description.Font = new System.Drawing.Font("Verdana", 10.25F);
            this._description.ForeColor = System.Drawing.Color.Black;
            this._description.Location = new System.Drawing.Point(198, 0);
            this._description.Name = "_description";
            this._description.Size = new System.Drawing.Size(490, 36);
            this._description.TabIndex = 8;
            this._description.Text = "Modulo Ventas  :  Conexión 192.168.1.200  SportStore  :  T/C 3.56  :  I.G.V. 0.18" +
    "";
            this._description.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // divControl
            // 
            this.divControl.Controls.Add(this.cmdExit);
            this.divControl.Controls.Add(this.cmdMinimize);
            this.divControl.Dock = System.Windows.Forms.DockStyle.Right;
            this.divControl.Location = new System.Drawing.Point(688, 0);
            this.divControl.Name = "divControl";
            this.divControl.Size = new System.Drawing.Size(74, 36);
            this.divControl.TabIndex = 10;
            // 
            // cmdExit
            // 
            this.cmdExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdExit.Font = new System.Drawing.Font("Verdana", 15F);
            this.cmdExit.ForeColor = System.Drawing.Color.Black;
            this.cmdExit.Location = new System.Drawing.Point(40, 3);
            this.cmdExit.Name = "cmdExit";
            this.cmdExit.Size = new System.Drawing.Size(28, 27);
            this.cmdExit.TabIndex = 11;
            this.cmdExit.Text = "X";
            this.cmdExit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cmdExit.Click += new System.EventHandler(this.cmdExit_Click);
            // 
            // cmdMinimize
            // 
            this.cmdMinimize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdMinimize.Font = new System.Drawing.Font("Verdana", 15F);
            this.cmdMinimize.ForeColor = System.Drawing.Color.Black;
            this.cmdMinimize.Location = new System.Drawing.Point(6, 3);
            this.cmdMinimize.Name = "cmdMinimize";
            this.cmdMinimize.Size = new System.Drawing.Size(28, 27);
            this.cmdMinimize.TabIndex = 10;
            this.cmdMinimize.Text = "-";
            this.cmdMinimize.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cmdMinimize.Click += new System.EventHandler(this.cmdMinimize_Click);
            // 
            // _title
            // 
            this._title.Dock = System.Windows.Forms.DockStyle.Left;
            this._title.Font = new System.Drawing.Font("Verdana", 15F);
            this._title.ForeColor = System.Drawing.Color.Black;
            this._title.Location = new System.Drawing.Point(0, 0);
            this._title.Name = "_title";
            this._title.Size = new System.Drawing.Size(198, 36);
            this._title.TabIndex = 7;
            this._title.Text = "Autonomus SDK";
            this._title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this._title.Visible = false;
            this._title.Click += new System.EventHandler(this._title_Click);
            // 
            // DivTemplate
            // 
            this.DivTemplate.Controls.Add(this.DivContainer);
            this.DivTemplate.Controls.Add(this._right);
            this.DivTemplate.Controls.Add(this._bottom);
            this.DivTemplate.Controls.Add(this._left);
            this.DivTemplate.Controls.Add(this._top);
            this.DivTemplate.Controls.Add(this.DivTopMenu);
            this.DivTemplate.Controls.Add(this.DivLeftSlide);
            this.DivTemplate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DivTemplate.Location = new System.Drawing.Point(0, 0);
            this.DivTemplate.Name = "DivTemplate";
            this.DivTemplate.Size = new System.Drawing.Size(998, 699);
            this.DivTemplate.TabIndex = 0;
            // 
            // DivContainer
            // 
            this.DivContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DivContainer.Location = new System.Drawing.Point(246, 53);
            this.DivContainer.Name = "DivContainer";
            this.DivContainer.Size = new System.Drawing.Size(742, 640);
            this.DivContainer.TabIndex = 3;
            // 
            // _right
            // 
            this._right.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this._right.Dock = System.Windows.Forms.DockStyle.Right;
            this._right.Location = new System.Drawing.Point(988, 53);
            this._right.Name = "_right";
            this._right.Size = new System.Drawing.Size(10, 640);
            this._right.TabIndex = 2;
            // 
            // _bottom
            // 
            this._bottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this._bottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._bottom.Location = new System.Drawing.Point(246, 693);
            this._bottom.Name = "_bottom";
            this._bottom.Size = new System.Drawing.Size(752, 6);
            this._bottom.TabIndex = 2;
            // 
            // _left
            // 
            this._left.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this._left.Dock = System.Windows.Forms.DockStyle.Left;
            this._left.Location = new System.Drawing.Point(236, 53);
            this._left.Name = "_left";
            this._left.Size = new System.Drawing.Size(10, 646);
            this._left.TabIndex = 2;
            // 
            // _top
            // 
            this._top.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this._top.Dock = System.Windows.Forms.DockStyle.Top;
            this._top.Location = new System.Drawing.Point(236, 36);
            this._top.Name = "_top";
            this._top.Size = new System.Drawing.Size(762, 17);
            this._top.TabIndex = 2;
            // 
            // DivLeftSlide
            // 
            this.DivLeftSlide.Controls.Add(this.DivSlideContainer);
            this.DivLeftSlide.Controls.Add(this.separRight);
            this.DivLeftSlide.Controls.Add(this.separatorSlide);
            this.DivLeftSlide.Controls.Add(this._modules);
            this.DivLeftSlide.Controls.Add(this.DivTopSlideLogo);
            this.DivLeftSlide.Controls.Add(this.DivBottomSlideUser);
            this.DivLeftSlide.Dock = System.Windows.Forms.DockStyle.Left;
            this.DivLeftSlide.Location = new System.Drawing.Point(0, 0);
            this.DivLeftSlide.Name = "DivLeftSlide";
            this.DivLeftSlide.Size = new System.Drawing.Size(236, 699);
            this.DivLeftSlide.TabIndex = 1;
            // 
            // separatorSlide
            // 
            this.separatorSlide.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.separatorSlide.Dock = System.Windows.Forms.DockStyle.Right;
            this.separatorSlide.Location = new System.Drawing.Point(234, 128);
            this.separatorSlide.Name = "separatorSlide";
            this.separatorSlide.Size = new System.Drawing.Size(2, 512);
            this.separatorSlide.TabIndex = 4;
            // 
            // _modules
            // 
            this._modules.Dock = System.Windows.Forms.DockStyle.Top;
            this._modules.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._modules.ForeColor = System.Drawing.Color.Silver;
            this._modules.Location = new System.Drawing.Point(0, 97);
            this._modules.Name = "_modules";
            this._modules.Size = new System.Drawing.Size(236, 31);
            this._modules.TabIndex = 5;
            this._modules.Text = "MÓDULOS DE SISTEMAS";
            this._modules.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // DivTopSlideLogo
            // 
            this.DivTopSlideLogo.Controls.Add(this.SubTitle);
            this.DivTopSlideLogo.Controls.Add(this.Title);
            this.DivTopSlideLogo.Controls.Add(this.Logo);
            this.DivTopSlideLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.DivTopSlideLogo.Location = new System.Drawing.Point(0, 0);
            this.DivTopSlideLogo.Name = "DivTopSlideLogo";
            this.DivTopSlideLogo.Size = new System.Drawing.Size(236, 97);
            this.DivTopSlideLogo.TabIndex = 2;
            // 
            // SubTitle
            // 
            this.SubTitle.Font = new System.Drawing.Font("Verdana", 8F);
            this.SubTitle.ForeColor = System.Drawing.Color.Black;
            this.SubTitle.Location = new System.Drawing.Point(6, 77);
            this.SubTitle.Name = "SubTitle";
            this.SubTitle.Size = new System.Drawing.Size(225, 14);
            this.SubTitle.TabIndex = 7;
            this.SubTitle.Text = "Sistema de gestión de aplicaciones";
            this.SubTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Title
            // 
            this.Title.Font = new System.Drawing.Font("Verdana", 13F);
            this.Title.ForeColor = System.Drawing.Color.Black;
            this.Title.Location = new System.Drawing.Point(6, 56);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(225, 22);
            this.Title.TabIndex = 6;
            this.Title.Text = "Autonomus SDK";
            this.Title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DivBottomSlideUser
            // 
            this.DivBottomSlideUser.Controls.Add(this._version);
            this.DivBottomSlideUser.Controls.Add(this._usuario);
            this.DivBottomSlideUser.Controls.Add(this.pictureBox1);
            this.DivBottomSlideUser.Controls.Add(this._rol);
            this.DivBottomSlideUser.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.DivBottomSlideUser.Location = new System.Drawing.Point(0, 640);
            this.DivBottomSlideUser.Name = "DivBottomSlideUser";
            this.DivBottomSlideUser.Size = new System.Drawing.Size(236, 59);
            this.DivBottomSlideUser.TabIndex = 3;
            // 
            // _version
            // 
            this._version.AutoSize = true;
            this._version.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._version.ForeColor = System.Drawing.Color.Gray;
            this._version.Location = new System.Drawing.Point(55, 40);
            this._version.Name = "_version";
            this._version.Size = new System.Drawing.Size(130, 13);
            this._version.TabIndex = 4;
            this._version.Text = "Versión: 2021.01.001";
            // 
            // _usuario
            // 
            this._usuario.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._usuario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this._usuario.Location = new System.Drawing.Point(55, 6);
            this._usuario.Name = "_usuario";
            this._usuario.Size = new System.Drawing.Size(173, 18);
            this._usuario.TabIndex = 2;
            this._usuario.Text = "Autonomus -SDK";
            // 
            // _rol
            // 
            this._rol.AutoSize = true;
            this._rol.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._rol.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this._rol.Location = new System.Drawing.Point(55, 24);
            this._rol.Name = "_rol";
            this._rol.Size = new System.Drawing.Size(97, 16);
            this._rol.TabIndex = 2;
            this._rol.Text = "Administrador";
            // 
            // Logo
            // 
            this.Logo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Logo.Image = ((System.Drawing.Image)(resources.GetObject("Logo.Image")));
            this.Logo.Location = new System.Drawing.Point(77, 3);
            this.Logo.Name = "Logo";
            this.Logo.Size = new System.Drawing.Size(82, 50);
            this.Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Logo.TabIndex = 5;
            this.Logo.TabStop = false;
            this.Logo.Click += new System.EventHandler(this.Logo_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(3, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(49, 43);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // DivSlideContainer
            // 
            this.DivSlideContainer.AutoScroll = true;
            this.DivSlideContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DivSlideContainer.Location = new System.Drawing.Point(6, 128);
            this.DivSlideContainer.Name = "DivSlideContainer";
            this.DivSlideContainer.Size = new System.Drawing.Size(228, 512);
            this.DivSlideContainer.TabIndex = 0;
            // 
            // separRight
            // 
            this.separRight.BackColor = System.Drawing.Color.White;
            this.separRight.Dock = System.Windows.Forms.DockStyle.Left;
            this.separRight.Location = new System.Drawing.Point(0, 128);
            this.separRight.Name = "separRight";
            this.separRight.Size = new System.Drawing.Size(6, 512);
            this.separRight.TabIndex = 6;
            // 
            // MenuSlideBar
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(998, 699);
            this.Controls.Add(this.DivTemplate);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "MenuSlideBar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MenuSlideBar";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MenuSlideBar_FormClosing);
            this.Load += new System.EventHandler(this.MenuSlideBar_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MenuSlideBar_KeyPress);
            this.DivTopMenu.ResumeLayout(false);
            this.divControl.ResumeLayout(false);
            this.DivTemplate.ResumeLayout(false);
            this.DivLeftSlide.ResumeLayout(false);
            this.DivTopSlideLogo.ResumeLayout(false);
            this.DivBottomSlideUser.ResumeLayout(false);
            this.DivBottomSlideUser.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel DivTopMenu;
        private System.Windows.Forms.Panel DivTemplate;
        private System.Windows.Forms.Panel DivBottomSlideUser;
        private System.Windows.Forms.Panel DivTopSlideLogo;
        private System.Windows.Forms.Label separatorSlide;
        private System.Windows.Forms.Label _modules;
        private System.Windows.Forms.Label _version;
        private System.Windows.Forms.Label _usuario;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label _rol;
        public System.Windows.Forms.Label SubTitle;
        public System.Windows.Forms.Label Title;
        public System.Windows.Forms.PictureBox Logo;
        public System.Windows.Forms.Label _title;
        public System.Windows.Forms.Label _description;
        private System.Windows.Forms.Label _right;
        private System.Windows.Forms.Label _bottom;
        private System.Windows.Forms.Label _left;
        private System.Windows.Forms.Label _top;
        public System.Windows.Forms.Panel DivContainer;
        public System.Windows.Forms.Panel DivLeftSlide;
        private System.Windows.Forms.Panel divControl;
        public System.Windows.Forms.Label cmdMinimize;
        public System.Windows.Forms.Label cmdExit;
        public System.Windows.Forms.Panel DivSlideContainer;
        private System.Windows.Forms.Label separRight;
    }
}