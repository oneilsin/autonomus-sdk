﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autonomo.CustomTemplate
{
    public partial class NavbarTopPanel : Form
    {
        Color _lineColor;
        Color _topColor;
        bool _moveForm;
        public NavbarTopPanel()
        {
            InitializeComponent();
            _lineColor = PanelTop.BackColor;
            TopLine.BackColor = _lineColor;
            _moveForm = false;
            _topColor = PanelTop.BackColor;
        }

        [Category("Autonomo properties")]
        [Description("Change: True = Move formulary and False: Disabled move formulary.")]
        public bool MoveFormumary
        {
            get { return _moveForm; }
            set { _moveForm = value; }
        }

        [Category("Autonomo properties")]
        public Color GridLineColor
        {
            get { return _lineColor; }
            set
            {
                _lineColor = value;
                TopLine.BackColor = _lineColor;
            }
        }

        [Category("Autonomo properties")]
        public Color TopBackColor
        {
            get { return _topColor; }
            set
            {
                _topColor = value;
                PanelTop.BackColor = _topColor;
            }
        }

        private void NavbarTopPanel_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r' || e.KeyChar == (char)Keys.Escape)
                e.Handled = true;
        }
    }
}
