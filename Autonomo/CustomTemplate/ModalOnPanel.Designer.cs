﻿
namespace Autonomo.CustomTemplate
{
    partial class ModalOnPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CustomContainer = new System.Windows.Forms.Panel();
            this.PanelControls = new System.Windows.Forms.Panel();
            this.PanelBottom = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.PanelTop = new System.Windows.Forms.Panel();
            this.PanelTitle = new System.Windows.Forms.Panel();
            this.FormTitle = new System.Windows.Forms.Label();
            this.FormCloseControl = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.CustomContainer.SuspendLayout();
            this.PanelBottom.SuspendLayout();
            this.PanelTop.SuspendLayout();
            this.PanelTitle.SuspendLayout();
            this.SuspendLayout();
            // 
            // CustomContainer
            // 
            this.CustomContainer.Controls.Add(this.PanelControls);
            this.CustomContainer.Controls.Add(this.PanelBottom);
            this.CustomContainer.Controls.Add(this.PanelTop);
            this.CustomContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CustomContainer.Location = new System.Drawing.Point(0, 0);
            this.CustomContainer.Name = "CustomContainer";
            this.CustomContainer.Size = new System.Drawing.Size(350, 450);
            this.CustomContainer.TabIndex = 1;
            // 
            // PanelControls
            // 
            this.PanelControls.AutoScroll = true;
            this.PanelControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelControls.Location = new System.Drawing.Point(0, 47);
            this.PanelControls.Name = "PanelControls";
            this.PanelControls.Size = new System.Drawing.Size(350, 346);
            this.PanelControls.TabIndex = 1;
            // 
            // PanelBottom
            // 
            this.PanelBottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            this.PanelBottom.Controls.Add(this.btnSave);
            this.PanelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelBottom.Location = new System.Drawing.Point(0, 393);
            this.PanelBottom.Name = "PanelBottom";
            this.PanelBottom.Size = new System.Drawing.Size(350, 57);
            this.PanelBottom.TabIndex = 2;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(93, 9);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(155, 36);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Guardar Cambios";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = false;
            // 
            // PanelTop
            // 
            this.PanelTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            this.PanelTop.Controls.Add(this.PanelTitle);
            this.PanelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelTop.Location = new System.Drawing.Point(0, 0);
            this.PanelTop.Name = "PanelTop";
            this.PanelTop.Size = new System.Drawing.Size(350, 47);
            this.PanelTop.TabIndex = 0;
            this.PanelTop.MouseDown += new System.Windows.Forms.MouseEventHandler(this.label1_MouseDown);
            // 
            // PanelTitle
            // 
            this.PanelTitle.Controls.Add(this.FormTitle);
            this.PanelTitle.Controls.Add(this.FormCloseControl);
            this.PanelTitle.Controls.Add(this.label1);
            this.PanelTitle.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelTitle.Location = new System.Drawing.Point(0, 16);
            this.PanelTitle.Name = "PanelTitle";
            this.PanelTitle.Size = new System.Drawing.Size(350, 31);
            this.PanelTitle.TabIndex = 0;
            // 
            // FormTitle
            // 
            this.FormTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FormTitle.Font = new System.Drawing.Font("Verdana", 13F);
            this.FormTitle.Location = new System.Drawing.Point(13, 0);
            this.FormTitle.Name = "FormTitle";
            this.FormTitle.Size = new System.Drawing.Size(302, 31);
            this.FormTitle.TabIndex = 0;
            this.FormTitle.Text = "Title";
            this.FormTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.FormTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.label1_MouseDown);
            // 
            // FormCloseControl
            // 
            this.FormCloseControl.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FormCloseControl.Dock = System.Windows.Forms.DockStyle.Right;
            this.FormCloseControl.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormCloseControl.Location = new System.Drawing.Point(315, 0);
            this.FormCloseControl.Name = "FormCloseControl";
            this.FormCloseControl.Size = new System.Drawing.Size(35, 31);
            this.FormCloseControl.TabIndex = 1;
            this.FormCloseControl.Text = "X";
            this.FormCloseControl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.FormCloseControl.Click += new System.EventHandler(this.FormCloseControl_Click);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 31);
            this.label1.TabIndex = 0;
            this.label1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.label1_MouseDown);
            // 
            // ModalOnPanel
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(350, 450);
            this.Controls.Add(this.CustomContainer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "ModalOnPanel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ModalOnPanel";
            this.Load += new System.EventHandler(this.ModalOnPanel_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ModalOnPanel_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ModalOnPanel_KeyPress);
            this.CustomContainer.ResumeLayout(false);
            this.PanelBottom.ResumeLayout(false);
            this.PanelTop.ResumeLayout(false);
            this.PanelTitle.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel CustomContainer;
        public System.Windows.Forms.Panel PanelControls;
        private System.Windows.Forms.Panel PanelBottom;
        public System.Windows.Forms.Panel PanelTop;
        private System.Windows.Forms.Panel PanelTitle;
        public System.Windows.Forms.Label FormTitle;
        public System.Windows.Forms.Label FormCloseControl;
        public System.Windows.Forms.Button btnSave;
        public System.Windows.Forms.Label label1;
    }
}