﻿
namespace Autonomo.CustomTemplate
{
    partial class CustomLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomLogin));
            this.ContenedorGeneral = new System.Windows.Forms.Panel();
            this._panelAccess = new System.Windows.Forms.Panel();
            this.btnAccess = new Autonomo.CustomControls.CustomButton();
            this.txPassword = new Autonomo.CustomControls.FlatTextBox();
            this.txUsuario = new Autonomo.CustomControls.FlatTextBox();
            this._panelModule = new System.Windows.Forms.Panel();
            this._panelCommands = new System.Windows.Forms.Panel();
            this._previous = new System.Windows.Forms.Label();
            this._next = new System.Windows.Forms.Label();
            this.cbModulo = new Autonomo.CustomControls.FlatComboBox();
            this._userName = new System.Windows.Forms.Label();
            this._question = new System.Windows.Forms.Label();
            this._version = new System.Windows.Forms.Label();
            this._panelLogo = new System.Windows.Forms.Panel();
            this._logo = new System.Windows.Forms.PictureBox();
            this._description = new System.Windows.Forms.Label();
            this._panelTitle = new System.Windows.Forms.Panel();
            this.btnClose = new Autonomo.CustomControls.CustomButton();
            this._topderecha = new System.Windows.Forms.Label();
            this._title = new System.Windows.Forms.Label();
            this._topIzquierda = new System.Windows.Forms.Label();
            this.CusMessage = new System.Windows.Forms.Label();
            this.ContenedorGeneral.SuspendLayout();
            this._panelAccess.SuspendLayout();
            this._panelModule.SuspendLayout();
            this._panelCommands.SuspendLayout();
            this._panelLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._logo)).BeginInit();
            this._panelTitle.SuspendLayout();
            this.SuspendLayout();
            // 
            // ContenedorGeneral
            // 
            this.ContenedorGeneral.Controls.Add(this._panelAccess);
            this.ContenedorGeneral.Controls.Add(this._panelModule);
            this.ContenedorGeneral.Controls.Add(this._version);
            this.ContenedorGeneral.Controls.Add(this._panelLogo);
            this.ContenedorGeneral.Controls.Add(this._panelTitle);
            this.ContenedorGeneral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ContenedorGeneral.Location = new System.Drawing.Point(0, 0);
            this.ContenedorGeneral.Name = "ContenedorGeneral";
            this.ContenedorGeneral.Size = new System.Drawing.Size(273, 634);
            this.ContenedorGeneral.TabIndex = 0;
            // 
            // _panelAccess
            // 
            this._panelAccess.Controls.Add(this.CusMessage);
            this._panelAccess.Controls.Add(this.btnAccess);
            this._panelAccess.Controls.Add(this.txPassword);
            this._panelAccess.Controls.Add(this.txUsuario);
            this._panelAccess.Location = new System.Drawing.Point(3, 202);
            this._panelAccess.Name = "_panelAccess";
            this._panelAccess.Size = new System.Drawing.Size(258, 196);
            this._panelAccess.TabIndex = 0;
            // 
            // btnAccess
            // 
            this.btnAccess.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.btnAccess.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAccess.FlatAppearance.BorderSize = 0;
            this.btnAccess.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAccess.Font = new System.Drawing.Font("Verdana", 10F);
            this.btnAccess.ForeColor = System.Drawing.Color.White;
            this.btnAccess.Image = ((System.Drawing.Image)(resources.GetObject("btnAccess.Image")));
            this.btnAccess.Location = new System.Drawing.Point(15, 117);
            this.btnAccess.Name = "btnAccess";
            this.btnAccess.Size = new System.Drawing.Size(220, 55);
            this.btnAccess.TabIndex = 2;
            this.btnAccess.Text = "Validar accesos";
            this.btnAccess.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAccess.UseVisualStyleBackColor = false;
            this.btnAccess.Click += new System.EventHandler(this.btnAccess_Click);
            // 
            // txPassword
            // 
            this.txPassword.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txPassword.BackColor = System.Drawing.Color.White;
            this.txPassword.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txPassword.ColorFocus = System.Drawing.Color.MediumSeaGreen;
            this.txPassword.ColorLine = System.Drawing.Color.Gray;
            this.txPassword.ColorText = System.Drawing.SystemColors.WindowText;
            this.txPassword.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txPassword.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txPassword.Error = "";
            this.txPassword.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txPassword.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txPassword.FormatLogin = true;
            this.txPassword.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txPassword.ImageIcon")));
            this.txPassword.Info = "";
            this.txPassword.Location = new System.Drawing.Point(15, 67);
            this.txPassword.MaterialStyle = true;
            this.txPassword.MaxLength = 30;
            this.txPassword.MultiLineText = false;
            this.txPassword.Name = "txPassword";
            this.txPassword.PasswordChar = '•';
            this.txPassword.Placeholder = "Password";
            this.txPassword.ReadOnly = false;
            this.txPassword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txPassword.Size = new System.Drawing.Size(220, 58);
            this.txPassword.SizeLine = 2;
            this.txPassword.TabIndex = 1;
            this.txPassword.Title = "Password";
            this.txPassword.VisibleIcon = true;
            this.txPassword.VisibleTitle = false;
            // 
            // txUsuario
            // 
            this.txUsuario.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txUsuario.BackColor = System.Drawing.Color.White;
            this.txUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txUsuario.ColorFocus = System.Drawing.Color.MediumSeaGreen;
            this.txUsuario.ColorLine = System.Drawing.Color.Gray;
            this.txUsuario.ColorText = System.Drawing.SystemColors.WindowText;
            this.txUsuario.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txUsuario.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txUsuario.Error = "";
            this.txUsuario.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txUsuario.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txUsuario.FormatLogin = true;
            this.txUsuario.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txUsuario.ImageIcon")));
            this.txUsuario.Info = "";
            this.txUsuario.Location = new System.Drawing.Point(15, 3);
            this.txUsuario.MaterialStyle = true;
            this.txUsuario.MaxLength = 30;
            this.txUsuario.MultiLineText = false;
            this.txUsuario.Name = "txUsuario";
            this.txUsuario.PasswordChar = '\0';
            this.txUsuario.Placeholder = "User name";
            this.txUsuario.ReadOnly = false;
            this.txUsuario.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txUsuario.Size = new System.Drawing.Size(220, 58);
            this.txUsuario.SizeLine = 2;
            this.txUsuario.TabIndex = 0;
            this.txUsuario.Title = "User name";
            this.txUsuario.VisibleIcon = true;
            this.txUsuario.VisibleTitle = false;
            // 
            // _panelModule
            // 
            this._panelModule.Controls.Add(this._panelCommands);
            this._panelModule.Controls.Add(this.cbModulo);
            this._panelModule.Controls.Add(this._userName);
            this._panelModule.Controls.Add(this._question);
            this._panelModule.Location = new System.Drawing.Point(3, 404);
            this._panelModule.Name = "_panelModule";
            this._panelModule.Size = new System.Drawing.Size(258, 160);
            this._panelModule.TabIndex = 0;
            this._panelModule.Visible = false;
            // 
            // _panelCommands
            // 
            this._panelCommands.Controls.Add(this._previous);
            this._panelCommands.Controls.Add(this._next);
            this._panelCommands.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._panelCommands.Location = new System.Drawing.Point(0, 131);
            this._panelCommands.Name = "_panelCommands";
            this._panelCommands.Size = new System.Drawing.Size(258, 29);
            this._panelCommands.TabIndex = 1;
            // 
            // _previous
            // 
            this._previous.Cursor = System.Windows.Forms.Cursors.Hand;
            this._previous.Dock = System.Windows.Forms.DockStyle.Left;
            this._previous.Font = new System.Drawing.Font("Verdana", 11F);
            this._previous.Image = ((System.Drawing.Image)(resources.GetObject("_previous.Image")));
            this._previous.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._previous.Location = new System.Drawing.Point(0, 0);
            this._previous.Name = "_previous";
            this._previous.Size = new System.Drawing.Size(102, 29);
            this._previous.TabIndex = 0;
            this._previous.Text = "     Regresar";
            this._previous.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._previous.Click += new System.EventHandler(this._previous_Click);
            // 
            // _next
            // 
            this._next.Cursor = System.Windows.Forms.Cursors.Hand;
            this._next.Dock = System.Windows.Forms.DockStyle.Right;
            this._next.Font = new System.Drawing.Font("Verdana", 11F);
            this._next.Image = ((System.Drawing.Image)(resources.GetObject("_next.Image")));
            this._next.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._next.Location = new System.Drawing.Point(155, 0);
            this._next.Name = "_next";
            this._next.Size = new System.Drawing.Size(103, 29);
            this._next.TabIndex = 0;
            this._next.Text = "Continuar";
            this._next.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbModulo
            // 
            this.cbModulo.BackColor = System.Drawing.Color.White;
            this.cbModulo.ColorFocus = System.Drawing.Color.MediumSeaGreen;
            this.cbModulo.ColorLine = System.Drawing.Color.Gray;
            this.cbModulo.ColorText = System.Drawing.SystemColors.WindowText;
            this.cbModulo.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbModulo.DisplayMember = "";
            this.cbModulo.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.cbModulo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbModulo.Error = "";
            this.cbModulo.FontText = new System.Drawing.Font("Verdana", 10F);
            this.cbModulo.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.cbModulo.ImageIcon = null;
            this.cbModulo.Info = "";
            this.cbModulo.Location = new System.Drawing.Point(9, 49);
            this.cbModulo.MaterialStyle = false;
            this.cbModulo.Name = "cbModulo";
            this.cbModulo.Placeholder = "";
            this.cbModulo.SelectedIndex = -1;
            this.cbModulo.Size = new System.Drawing.Size(239, 58);
            this.cbModulo.SizeLine = 2;
            this.cbModulo.TabIndex = 0;
            this.cbModulo.Title = "-select- modules";
            this.cbModulo.ValueMember = "";
            this.cbModulo.VisibleIcon = false;
            this.cbModulo.VisibleTitle = true;
            // 
            // _userName
            // 
            this._userName.AutoSize = true;
            this._userName.Font = new System.Drawing.Font("Verdana", 11F);
            this._userName.Location = new System.Drawing.Point(12, 16);
            this._userName.Name = "_userName";
            this._userName.Size = new System.Drawing.Size(40, 18);
            this._userName.TabIndex = 6;
            this._userName.Text = "user";
            // 
            // _question
            // 
            this._question.AutoSize = true;
            this._question.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._question.Location = new System.Drawing.Point(9, 0);
            this._question.Name = "_question";
            this._question.Size = new System.Drawing.Size(83, 16);
            this._question.TabIndex = 5;
            this._question.Text = "Bienvenido!";
            // 
            // _version
            // 
            this._version.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._version.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._version.Location = new System.Drawing.Point(0, 609);
            this._version.Name = "_version";
            this._version.Size = new System.Drawing.Size(273, 25);
            this._version.TabIndex = 3;
            this._version.Text = "version: {0}";
            this._version.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // _panelLogo
            // 
            this._panelLogo.Controls.Add(this._logo);
            this._panelLogo.Controls.Add(this._description);
            this._panelLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this._panelLogo.Location = new System.Drawing.Point(0, 37);
            this._panelLogo.Name = "_panelLogo";
            this._panelLogo.Size = new System.Drawing.Size(273, 159);
            this._panelLogo.TabIndex = 0;
            // 
            // _logo
            // 
            this._logo.Dock = System.Windows.Forms.DockStyle.Fill;
            this._logo.Image = ((System.Drawing.Image)(resources.GetObject("_logo.Image")));
            this._logo.Location = new System.Drawing.Point(0, 0);
            this._logo.Name = "_logo";
            this._logo.Size = new System.Drawing.Size(273, 117);
            this._logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this._logo.TabIndex = 0;
            this._logo.TabStop = false;
            this._logo.MouseDown += new System.Windows.Forms.MouseEventHandler(this._logo_MouseDown);
            // 
            // _description
            // 
            this._description.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._description.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._description.Location = new System.Drawing.Point(0, 117);
            this._description.Name = "_description";
            this._description.Size = new System.Drawing.Size(273, 42);
            this._description.TabIndex = 2;
            this._description.Text = "Description\r\ndetail";
            this._description.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _panelTitle
            // 
            this._panelTitle.BackColor = System.Drawing.Color.White;
            this._panelTitle.Controls.Add(this.btnClose);
            this._panelTitle.Controls.Add(this._topderecha);
            this._panelTitle.Controls.Add(this._title);
            this._panelTitle.Controls.Add(this._topIzquierda);
            this._panelTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this._panelTitle.Location = new System.Drawing.Point(0, 0);
            this._panelTitle.Name = "_panelTitle";
            this._panelTitle.Size = new System.Drawing.Size(273, 37);
            this._panelTitle.TabIndex = 0;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.White;
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Verdana", 10F);
            this.btnClose.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnClose.Location = new System.Drawing.Point(233, 0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(34, 37);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "X";
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // _topderecha
            // 
            this._topderecha.Dock = System.Windows.Forms.DockStyle.Right;
            this._topderecha.Location = new System.Drawing.Point(267, 0);
            this._topderecha.Name = "_topderecha";
            this._topderecha.Size = new System.Drawing.Size(6, 37);
            this._topderecha.TabIndex = 3;
            // 
            // _title
            // 
            this._title.Cursor = System.Windows.Forms.Cursors.Hand;
            this._title.Dock = System.Windows.Forms.DockStyle.Fill;
            this._title.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold);
            this._title.Location = new System.Drawing.Point(6, 0);
            this._title.Name = "_title";
            this._title.Size = new System.Drawing.Size(267, 37);
            this._title.TabIndex = 3;
            this._title.Text = "Title";
            this._title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._title.MouseDown += new System.Windows.Forms.MouseEventHandler(this._title_MouseDown);
            // 
            // _topIzquierda
            // 
            this._topIzquierda.Dock = System.Windows.Forms.DockStyle.Left;
            this._topIzquierda.Location = new System.Drawing.Point(0, 0);
            this._topIzquierda.Name = "_topIzquierda";
            this._topIzquierda.Size = new System.Drawing.Size(6, 37);
            this._topIzquierda.TabIndex = 3;
            // 
            // CusMessage
            // 
            this.CusMessage.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.CusMessage.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CusMessage.Location = new System.Drawing.Point(0, 175);
            this.CusMessage.Name = "CusMessage";
            this.CusMessage.Size = new System.Drawing.Size(258, 21);
            this.CusMessage.TabIndex = 6;
            this.CusMessage.Text = "...";
            this.CusMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CustomLogin
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(273, 634);
            this.Controls.Add(this.ContenedorGeneral);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "CustomLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CustomLogin";
            this.Load += new System.EventHandler(this.CustomLogin_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CustomLogin_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CustomLogin_KeyPress);
            this.ContenedorGeneral.ResumeLayout(false);
            this._panelAccess.ResumeLayout(false);
            this._panelModule.ResumeLayout(false);
            this._panelModule.PerformLayout();
            this._panelCommands.ResumeLayout(false);
            this._panelLogo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._logo)).EndInit();
            this._panelTitle.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel ContenedorGeneral;
        private CustomControls.CustomButton btnClose;
        private System.Windows.Forms.Panel _panelTitle;
        private System.Windows.Forms.Label _topderecha;
        private System.Windows.Forms.Label _title;
        private System.Windows.Forms.Label _topIzquierda;
        private System.Windows.Forms.Panel _panelLogo;
        private System.Windows.Forms.PictureBox _logo;
        private System.Windows.Forms.Label _description;
        private System.Windows.Forms.Label _version;
        private System.Windows.Forms.Panel _panelModule;
        private System.Windows.Forms.Panel _panelAccess;
        public CustomControls.CustomButton btnAccess;
        public CustomControls.FlatTextBox txPassword;
        public CustomControls.FlatTextBox txUsuario;
        private System.Windows.Forms.Label _userName;
        private System.Windows.Forms.Label _question;
        public CustomControls.FlatComboBox cbModulo;
        private System.Windows.Forms.Label _next;
        private System.Windows.Forms.Label _previous;
        private System.Windows.Forms.Panel _panelCommands;
        public System.Windows.Forms.Label CusMessage;
    }
}