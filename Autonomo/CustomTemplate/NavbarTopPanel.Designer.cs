﻿
namespace Autonomo.CustomTemplate
{
    partial class NavbarTopPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PanelTop = new System.Windows.Forms.Panel();
            this.PanelFinder = new System.Windows.Forms.Panel();
            this._space = new System.Windows.Forms.Label();
            this.TopLine = new System.Windows.Forms.Label();
            this.SpaceFinder = new System.Windows.Forms.Label();
            this.PanelTitle = new System.Windows.Forms.Panel();
            this.FormTitle = new System.Windows.Forms.Label();
            this.CustomContainer = new System.Windows.Forms.Panel();
            this.PanelTop.SuspendLayout();
            this.PanelFinder.SuspendLayout();
            this.PanelTitle.SuspendLayout();
            this.SuspendLayout();
            // 
            // PanelTop
            // 
            this.PanelTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            this.PanelTop.Controls.Add(this.PanelFinder);
            this.PanelTop.Controls.Add(this.PanelTitle);
            this.PanelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelTop.Location = new System.Drawing.Point(0, 0);
            this.PanelTop.Name = "PanelTop";
            this.PanelTop.Size = new System.Drawing.Size(800, 86);
            this.PanelTop.TabIndex = 0;
            // 
            // PanelFinder
            // 
            this.PanelFinder.Controls.Add(this._space);
            this.PanelFinder.Controls.Add(this.TopLine);
            this.PanelFinder.Controls.Add(this.SpaceFinder);
            this.PanelFinder.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelFinder.Location = new System.Drawing.Point(0, 36);
            this.PanelFinder.Name = "PanelFinder";
            this.PanelFinder.Size = new System.Drawing.Size(800, 50);
            this.PanelFinder.TabIndex = 1;
            // 
            // _space
            // 
            this._space.Dock = System.Windows.Forms.DockStyle.Left;
            this._space.Location = new System.Drawing.Point(0, 4);
            this._space.Name = "_space";
            this._space.Size = new System.Drawing.Size(15, 44);
            this._space.TabIndex = 2;
            // 
            // TopLine
            // 
            this.TopLine.BackColor = System.Drawing.SystemColors.Highlight;
            this.TopLine.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TopLine.Location = new System.Drawing.Point(0, 48);
            this.TopLine.Name = "TopLine";
            this.TopLine.Size = new System.Drawing.Size(800, 2);
            this.TopLine.TabIndex = 3;
            // 
            // SpaceFinder
            // 
            this.SpaceFinder.Dock = System.Windows.Forms.DockStyle.Top;
            this.SpaceFinder.Location = new System.Drawing.Point(0, 0);
            this.SpaceFinder.Name = "SpaceFinder";
            this.SpaceFinder.Size = new System.Drawing.Size(800, 4);
            this.SpaceFinder.TabIndex = 4;
            // 
            // PanelTitle
            // 
            this.PanelTitle.Controls.Add(this.FormTitle);
            this.PanelTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelTitle.Location = new System.Drawing.Point(0, 0);
            this.PanelTitle.Name = "PanelTitle";
            this.PanelTitle.Size = new System.Drawing.Size(800, 33);
            this.PanelTitle.TabIndex = 0;
            // 
            // FormTitle
            // 
            this.FormTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FormTitle.Font = new System.Drawing.Font("Verdana", 15F);
            this.FormTitle.Location = new System.Drawing.Point(0, 0);
            this.FormTitle.Name = "FormTitle";
            this.FormTitle.Size = new System.Drawing.Size(800, 33);
            this.FormTitle.TabIndex = 0;
            this.FormTitle.Text = "Title";
            this.FormTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CustomContainer
            // 
            this.CustomContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CustomContainer.Location = new System.Drawing.Point(0, 86);
            this.CustomContainer.Name = "CustomContainer";
            this.CustomContainer.Size = new System.Drawing.Size(800, 364);
            this.CustomContainer.TabIndex = 1;
            // 
            // NavbarTopPanel
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.CustomContainer);
            this.Controls.Add(this.PanelTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "NavbarTopPanel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NavbarTopPanel";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NavbarTopPanel_KeyPress);
            this.PanelTop.ResumeLayout(false);
            this.PanelFinder.ResumeLayout(false);
            this.PanelTitle.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel PanelTop;
        private System.Windows.Forms.Label TopLine;
        public System.Windows.Forms.Label SpaceFinder;
        protected System.Windows.Forms.Panel PanelTitle;
        public System.Windows.Forms.Label FormTitle;
        public System.Windows.Forms.Label _space;
        public System.Windows.Forms.Panel CustomContainer;
        public System.Windows.Forms.Panel PanelFinder;
    }
}