﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autonomo.CustomControls
{
    public partial class AutonomusTextBox : Panel
    {
        Panel divImage, divControl, divError;
        PictureBox img;
        Label bottomLine;
        TextBox _textBox;
        Label _label, _error;
        Color _backColor;
        Color _foreColorCaption;

        public Image Image
        {
            get { return img.Image; }
            set { img.Image = value; }
        }

        void ConfigurationBottomLine()
        {
            bottomLine = new Label()
            {
                Text = "",
                BackColor = Color.Gray,
                AutoSize = false,
                Dock = DockStyle.Bottom,
                Height = 2
            };
            divControl.Controls.Add(bottomLine);
        }
        void ConfigurationImg()
        {
            img = new PictureBox()
            {
                SizeMode = PictureBoxSizeMode.Zoom,
                Image = null,
                Dock = DockStyle.Fill
            };
            divImage.Controls.Add(img);
        }

        void ConfigurationError()
        {
            _error = new Label()
            {
                Text = "dsadsadsad sadsadsad",//string.Empty,
                ForeColor = Color.Red,
                Font = new Font("Verdana", 8.5f),
                AutoSize=false,
                Dock = DockStyle.Fill,
                TextAlign= ContentAlignment.TopLeft
            };

            divError.Controls.Add(_error);
        }
        void ConfigurationDivImage()
        {
            divImage = new Panel()
            {
                Size = new Size(51, 46),
                BackColor = _backColor,
                Dock = DockStyle.Left
            };
            ConfigurationImg();
            base.Controls.Add(divImage);
            divImage.SendToBack();
        }

        void ConfigurationLabel()
        {
            _label = new Label()
            {
                Text = "Caption text",
                Font = new Font("Verdana", 10),
                Dock = DockStyle.Top,
                ForeColor = _foreColorCaption,
                AutoSize =true
            };
            divControl.Controls.Add(_label);
            _label.SendToBack();
        }
        void ConfigurationTextBox()
        {
            _textBox = new TextBox()
            {
                Text = string.Empty,
                BackColor = _backColor,
                BorderStyle = BorderStyle.None,
                Font = new Font("Verdana", 11),
                Dock = DockStyle.Bottom
            };
            _textBox.BringToFront();
            divControl.Controls.Add(_textBox);
        }
        void ConfigurationDivControl()
        {
            divControl = new Panel()
            {
                BackColor = _backColor,
                Dock = DockStyle.Fill
            };

            ConfigurationLabel();
            ConfigurationTextBox();
            ConfigurationBottomLine();
            base.Controls.Add(divControl);
            divControl.BringToFront();
        }

        void ConfigurationDivError()
        {
            divError = new Panel()
            {
                BackColor = _backColor,
                Dock = DockStyle.Bottom,
                Height=15
            };
            ConfigurationError();
            base.Controls.Add(divError);
            divError.SendToBack();
        }
        public AutonomusTextBox()
        {
            InitializeComponent();
            base.Size = new Size(278, 58);
            _backColor = Color.White;
            _foreColorCaption = Color.DimGray;

            ConfigurationDivError();
            ConfigurationDivImage();
            ConfigurationDivControl();
          
            _textBox.Focus();
        }

        protected override void OnClick(EventArgs e)
        {
            base.OnClick(e);
        }
    }
}
