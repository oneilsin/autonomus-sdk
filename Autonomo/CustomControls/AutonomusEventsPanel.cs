﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autonomo.CustomControls
{
    public partial class AutonomusEventsPanel : UserControl
    {
        Font _labelFont;
        Color _labelColor;
        Color _separatorColor;
        public AutonomusEventsPanel()
        {
            InitializeComponent();
            _labelFont = new Font("Verdana", 10f, FontStyle.Regular);
            _labelColor = Color.FromArgb(64, 64, 64);
            _separatorColor = Color.FromArgb(241, 241, 241);
        }

        //public string[] Items
        //{
        //    get { return; }
        //    set
        //    {

        //    }
        //}

        [Category("Autonomo properties")]
        public void AddItems(string value)
        {
            Color fc = value.Length > 2 ?
                (value.Substring(0, 1) == "*" ? Color.Red : _labelColor)
                : _labelColor;

            var label = new Label()
            {
                Font = _labelFont,
                ForeColor = fc,
                BackColor = Contenedor.BackColor,
                Image = Properties.Resources.telegram,
                ImageAlign = ContentAlignment.TopLeft,
                TextAlign = ContentAlignment.TopLeft,
                AutoSize = true
            };

            string t = string.Format(spaceText(label), value);
            label.Text = t;
            LabelContainers.Controls.Add(label);

            LabelContainers.Controls.Add(new Label()
            {
                BackColor = _separatorColor,
                AutoSize = false,
                Height = 1,
                Width = LabelContainers.Width - 24
            });
        }

        string spaceText(Label l)
        {
            string ret = "   {0}";
            float size = l.Font.Size;
            if (size >= 8 && size < 9)
                ret = "    {0}";//4
            if (size >= 9 && size < 12)
                ret = "   {0}";//3
            if (size >= 12)
                ret = "  {0}";//2
            return ret;
        }

        public void Clear()
        {
            LabelContainers.Controls.Clear();
        }

        [Category("Autonomo properties")]
        public string Title
        {
            get { return Titulo.Text; }
            set
            {
                value = value ?? string.Empty; // Validamos si el valor es nulo
                Titulo.Text = value;// Llenamos el Text con el valor final.
            }
        }

        [Category("Autonomo properties")]
        public Font FontTitle //Cambia el tipo de fuente de l Titulo
        {
            get { return Titulo.Font; }
            set { Titulo.Font = value; }
        }

        [Category("Autonomo properties")]
        public Color ColorTitle // Cambia el color del texto del Titulo
        {
            get { return Titulo.ForeColor; }
            set { Titulo.ForeColor = value; }
        }


        [Category("Autonomo properties")]
        public Font FontLabel //Cambia el tipo de fuente de l Titulo
        {
            get { return _labelFont; }
            set { _labelFont = value; }
        }

        [Category("Autonomo properties")]
        public Color ColorLabel // Cambia el color del texto del Titulo
        {
            get { return _labelColor; }
            set { _labelColor = value; }
        }

        [Category("Autonomo properties")]
        public Color ColorSeparador // Cambia el color del texto del Titulo
        {
            get { return _separatorColor; }
            set { _separatorColor = value; }
        }

        [Category("Autonomo properties")]
        public bool VisibleTitle
        {
            get { return titlePanel.Visible; }
            set { titlePanel.Visible = value; }
        }

        //[EditorBrowsable(EditorBrowsableState.Always)]
        //[Browsable(true)]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        //[Bindable(true)]
        //[Category("Autonomo properties")]
        //public override string Text // Muestra el contenido del TextBox
        //{
        //    get { return _label.Text; }
        //    set
        //    {
        //        value = value ?? string.Empty;
        //        _label.Text = value;
        //    }
        //}
    }
}
