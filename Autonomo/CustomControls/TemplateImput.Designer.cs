﻿namespace Autonomo.CustomControls
{
    partial class TemplateImput
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.divImage = new System.Windows.Forms.Panel();
            this.iconControl = new System.Windows.Forms.PictureBox();
            this.divControl = new System.Windows.Forms.Panel();
            this.lineBot = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.divImage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconControl)).BeginInit();
            this.divControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // divImage
            // 
            this.divImage.Controls.Add(this.iconControl);
            this.divImage.Dock = System.Windows.Forms.DockStyle.Left;
            this.divImage.Location = new System.Drawing.Point(0, 0);
            this.divImage.Name = "divImage";
            this.divImage.Size = new System.Drawing.Size(51, 46);
            this.divImage.TabIndex = 0;
            // 
            // iconControl
            // 
            this.iconControl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.iconControl.Cursor = System.Windows.Forms.Cursors.Hand;
            this.iconControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iconControl.Location = new System.Drawing.Point(0, 0);
            this.iconControl.Name = "iconControl";
            this.iconControl.Size = new System.Drawing.Size(51, 46);
            this.iconControl.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.iconControl.TabIndex = 0;
            this.iconControl.TabStop = false;
            // 
            // divControl
            // 
            this.divControl.Controls.Add(this.lineBot);
            this.divControl.Controls.Add(this.label1);
            this.divControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.divControl.Location = new System.Drawing.Point(51, 0);
            this.divControl.Name = "divControl";
            this.divControl.Size = new System.Drawing.Size(269, 46);
            this.divControl.TabIndex = 0;
            // 
            // lineBot
            // 
            this.lineBot.BackColor = System.Drawing.Color.LightGray;
            this.lineBot.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lineBot.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lineBot.Location = new System.Drawing.Point(0, 44);
            this.lineBot.Name = "lineBot";
            this.lineBot.Size = new System.Drawing.Size(269, 2);
            this.lineBot.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Caption text";
            // 
            // TemplateImput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.divControl);
            this.Controls.Add(this.divImage);
            this.Name = "TemplateImput";
            this.Size = new System.Drawing.Size(320, 46);
            this.divImage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.iconControl)).EndInit();
            this.divControl.ResumeLayout(false);
            this.divControl.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel divImage;
        private System.Windows.Forms.Panel divControl;
        private System.Windows.Forms.PictureBox iconControl;
        private System.Windows.Forms.Label lineBot;
        private System.Windows.Forms.Label label1;
    }
}
