﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Autonomo.CustomControls.HelperControl;

namespace Autonomo.CustomControls
{
    public partial class AutonomusModulePanel : Panel
    {
        Theme theme;
        Color _backColor;
        Color _backCaptionColor;
        Color _ForeToWhite;
        Color _ForeToDark;
        CheckBox _checkBox;
        private Image uncheckImage = Properties.Resources.DownToWhite_18px;
        private Image checkImage = Properties.Resources.UpToWhite_18px;

        private Image _image = Properties.Resources.Add20;
        string _text;
        int _captionHeight, _mainHeigt;
        Font _font;
        bool _check, _position;

        public AutonomusModulePanel()
        {
            _font = new Font("Verdana", 10);
            _captionHeight = 38;
            _mainHeigt = _captionHeight;// + 6;
            _check = false;
            _ForeToWhite = Color.FromArgb(64, 64, 64);
            _ForeToDark = Color.WhiteSmoke;

            InitializeCumtomPanel();
            _checkBox.BringToFront();
            theme = Theme.White;
        }



        void switchTheme()
        {
            switch (theme)
            {
                case Theme.White:
                    {
                        _checkBox.ForeColor = Color.FromArgb(64, 64, 64);
                        uncheckImage = Properties.Resources.DownToWhite_18px;
                        checkImage = Properties.Resources.UpToWhite_18px;
                        _checkBox.Image = uncheckImage;
                        break;
                    }
                case Theme.Dark:
                    {
                        _checkBox.ForeColor = Color.WhiteSmoke;
                        uncheckImage = Properties.Resources.DownToDark_18px;
                        checkImage = Properties.Resources.UpToDark_18px;
                        _checkBox.Image = uncheckImage;
                        break;
                    }
                case Theme.BlueDark:
                    break;
                case Theme.Green:
                    break;
            }

            if (this.Height > 60)
            {
                _checkBox.Image = checkImage;
            }
            if (_checkBox.Checked)
            {
                some();
            }
        }


        private void InitializeCumtomPanel()
        {
            _backColor = _backCaptionColor = base.BackColor;
            ConfigurationCheckBox();

            if (_checkBox != null)
            {
                switchTheme();
            }
        }
        private void ConfigurationCheckBox()
        {
            _checkBox = new CheckBox()
            {
                TextImageRelation = TextImageRelation.Overlay,
                Appearance = Appearance.Button,
                Cursor = Cursors.Hand,
                FlatStyle = FlatStyle.Flat,
                Font = this._font,
                Image = uncheckImage,
                BackgroundImage = _image,

                Text = _text,
                ImageAlign = ContentAlignment.MiddleRight,
                TextAlign = ContentAlignment.MiddleLeft,
                Dock = DockStyle.Top,
                AutoSize = false,
                Height = _captionHeight,
                // BackColor = _topColor,
                Checked = _check,
                CheckAlign = ContentAlignment.MiddleRight,
                BackgroundImageLayout = ImageLayout.None
            };
            _checkBox.FlatAppearance.BorderSize = 0;
            //some();

            _checkBox.CheckedChanged += new EventHandler(Ckeck_CheckedChanged);
            _checkBox.CheckedChanged += new EventHandler(Status_CheckedChanged);
            _checkBox.MouseEnter += new EventHandler(check_MouseEnter);
            _checkBox.MouseLeave += new EventHandler(check_MouseLeave);
            base.Controls.Add(_checkBox);
        }

        void some()
        {
            _checkBox.FlatAppearance.CheckedBackColor = BackCaptionColor;// Color.Transparent;
            _checkBox.FlatAppearance.MouseDownBackColor = BackCaptionColor;//Color.Transparent;
            _checkBox.FlatAppearance.MouseOverBackColor = BackCaptionColor;//Color.Transparent;

            _checkBox.ForeColor = ForeColorSelected;
        }
        Color ForeColorSelected
        {
            get
            {
                if (theme == Theme.Dark || theme == Theme.BlueDark)
                {
                    return _ForeToDark;
                }
                return _ForeToWhite;
            }
        }
        private void Ckeck_CheckedChanged(object sender, EventArgs e)
        {
            var value = _checkBox.Checked;
            _checkBox.Image = value ? checkImage : uncheckImage;
            _check = value;
            CheckDisplay();
        }
        private void check_MouseEnter(object sender, EventArgs e)
        {
            if (_checkBox != null)
            {
                _checkBox.Font = new Font(_font, FontStyle.Bold);
                //_checkBox.ForeColor = Color.Red;//  ForeColorSelected;
                some();
            }
        }

        private void check_MouseLeave(object sender, EventArgs e)
        {
            if (_checkBox != null)
            {
                _checkBox.Font = new Font(_font, FontStyle.Regular);
                //_checkBox.ForeColor = Color.Red;//ForeColorSelected;
                some();
            }
        }
        private void CheckDisplay()
        {
            if (!_check)
            {
                this.Height = _captionHeight;// + 4;
                return;
            }
            this.Height = _mainHeigt;
        }

        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Bindable(true)]
        [Category("Autonomo properties")]
        public override string Text // Muestra el contenido del TextBox
        {
            get { return _text; }
            set
            {
                value = value ?? string.Empty;
                _text = value;
                _checkBox.Text = $"      {_text}";
            }
        }

        [Category("Autonomo properties")]
        public override Font Font
        {
            get { return _font; }
            set
            {
                _font = value;
                _checkBox.Font = _font;
            }
        }

        [Category("Autonomo properties")]
        public override Color BackColor
        {
            get { return _backColor; }
            set
            {
                _backColor = value;
                base.BackColor = _backColor;
                //if (_checkBox != null)
                //    _checkBox.BackColor = _backColor;
            }
        }

        [Category("Autonomo properties")]
        public int CaptionHeight
        {
            get { return _captionHeight; }
            set
            {
                _captionHeight = value;
                _checkBox.Height = _captionHeight;
            }
        }

        [Category("Autonomo properties")]
        public bool Checked
        {
            get { return _checkBox.Checked; }
            set
            {
                _check = value;
                _checkBox.Checked = _check;
            }
        }

        [Category("Autonomo properties")]
        [Description("Tamaño del panel a expandir")]
        public int HeightPanel
        {
            get { return _mainHeigt; }
            set { _mainHeigt = value; }
        }

        [Category("Autonomo properties")]
        public event EventHandler CheckedChanged;
        private void Status_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckedChanged != null) CheckedChanged(sender, e);
        }

        [Category("Autonomo properties")]
        public bool PositionFront
        {
            get { return _position; }
            set
            {
                _position = value;
                if (_position)
                    _checkBox.BringToFront();
                else
                    _checkBox.SendToBack();
            }
        }

        [Category("Autonomo properties")]
        public Image Image
        {
            get { return _image; }
            set
            {
                _image = value;
                if (_checkBox != null)
                {
                    _checkBox.BackgroundImage = _image;
                }
            }
        }

        [Category("Autonomo properties")]
        public Color BackCaptionColor
        {
            get { return _backCaptionColor; }
            set
            {
                _backCaptionColor = value;
                if (_checkBox != null)
                    _checkBox.BackColor = _backCaptionColor;
            }
        }

        [Category("Autonomo properties")]
        public Theme Theme
        {
            get { return theme; }
            set
            {
                theme = value;

                if (_checkBox != null)
                    switchTheme();
            }
        }

        [Category("Autonomo properties")]
        public Color ForeToWhiteColor
        {
            get { return _ForeToWhite; }
            set
            {
                _ForeToWhite = value;
            }
        }

        [Category("Autonomo properties")]
        public Color ForeToDarKColor
        {
            get { return _ForeToDark; }
            set
            {
                _ForeToDark = value;
            }
        }
    }
}
