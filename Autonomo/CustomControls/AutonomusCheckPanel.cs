﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autonomo.CustomControls
{
    public class AutonomusCheckPanel : Panel
    {
        Color _backColor;
        CheckBox _checkBox;
        private Image uncheckImage = Properties.Resources.RadioUncheck;
        private Image checkImage = Properties.Resources.RadioCheck;
        string _text;
        int _captionHeight, _mainHeigt;
        Font _font;
        bool _check, _position;
        //Panel _container;

        public AutonomusCheckPanel()
        {
            _font = new Font("Verdana", 10);
            _captionHeight = 30;
            _mainHeigt = _captionHeight + 6;
            _check = false;
            InitializeCumtomPanel();
            _checkBox.BringToFront();
            // _container.BringToFront();
        }
        //private void ManageContainer()
        //{
        //    _container = new Panel()
        //    {
        //        BackColor = _backColor,
        //        Dock = DockStyle.Top,
        //        Height = _captionHeight,
        //    };
        //    _container.BringToFront();

        //    ConfigurationCheckBox();
        //    base.Controls.Add(_container);
        //}

        private void InitializeCumtomPanel()
        {
            this._backColor = base.BackColor;

            ConfigurationCheckBox();
            //  ManageContainer();
            //this.Paint += new PaintEventHandler(panel_Paint);
            //  MessageBox.Show($"init: {_mainHeigt}");
        }
        private void ConfigurationCheckBox()
        {
            _checkBox = new CheckBox()
            {
                Appearance = Appearance.Button,
                Cursor = Cursors.Hand,
                FlatStyle = FlatStyle.Flat,
                Font = this._font,
                Image = uncheckImage,
                TextImageRelation = TextImageRelation.ImageBeforeText,
                Text = _text,
                ImageAlign = ContentAlignment.MiddleLeft,
                TextAlign = ContentAlignment.MiddleLeft,
                Dock = DockStyle.Top,
                AutoSize = false,
                Height = _captionHeight,
                BackColor = _backColor,
                Checked = _check
            };
            _checkBox.FlatAppearance.BorderSize = 0;
            //_checkBox.FlatAppearance.MouseDownBackColor = Color.Transparent;
            //_checkBox.FlatAppearance.MouseOverBackColor = Color.Transparent;
            _checkBox.CheckedChanged += new EventHandler(Ckeck_CheckedChanged);
            _checkBox.CheckedChanged += new EventHandler(Status_CheckedChanged);
            base.Controls.Add(_checkBox);
            // _container.Controls.Add(_checkBox);
        }
        //private void panel_Paint(object sender, PaintEventArgs e)
        //{
        //   // base.Size = _check ? _size : new Size(260, 36);
        //}
        private void Ckeck_CheckedChanged(object sender, EventArgs e)
        {
            var value = _checkBox.Checked;
            _checkBox.Image = value ? checkImage : uncheckImage;
            _check = value;
            CheckDisplay();
        }
        private void CheckDisplay()
        {
            if (!_check)
            {
                this.Height = _captionHeight + 4;
                //  MessageBox.Show($"aa: {this.Height}");
                return;
            }
            this.Height = _mainHeigt;
            //  MessageBox.Show($"bb: {this.Height}");
        }

        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Bindable(true)]
        [Category("Autonomo properties")]
        public override string Text // Muestra el contenido del TextBox
        {
            get { return _text; }
            set
            {
                value = value ?? string.Empty;
                _text = value;
                _checkBox.Text = _text;
            }
        }

        [Category("Autonomo properties")]
        public override Font Font
        {
            get { return _font; }
            set
            {
                _font = value;
                _checkBox.Font = _font;
            }
        }

        [Category("Autonomo properties")]
        public override Color BackColor
        {
            get { return _backColor; }
            set
            {
                _backColor = value;
                base.BackColor = _backColor;
                _checkBox.BackColor = _backColor;
                //_container.BackColor = _backColor;
            }
        }

        [Category("Autonomo properties")]
        public int CaptionHeight
        {
            get { return _captionHeight; }
            set
            {
                _captionHeight = value;
                _checkBox.Height = _captionHeight;
            }
        }

        [Category("Autonomo properties")]
        public bool Checked
        {
            get { return _checkBox.Checked; }
            set
            {
                _check = value;
                _checkBox.Checked = _check;
            }
        }

        [Category("Autonomo properties")]
        public int HeightPanel
        {
            get { return _mainHeigt; }
            set { _mainHeigt = value; }
        }

        [Category("Autonomo properties")]
        public event EventHandler CheckedChanged;
        private void Status_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckedChanged != null) CheckedChanged(sender, e);
        }

        [Category("Autonomo properties")]
        public bool PositionFront
        {
            get { return _position; }
            set
            {
                _position = value;
                if (_position)
                    _checkBox.BringToFront();
                else
                    _checkBox.SendToBack();
            }
        }
        //public enum PositionObject { Back, Front }
        // [Category("Autonomo properties")]
        //public PositionObject Position
        //{
        //    get { return PositionObject.Front; }
        //    set { Position = value;  aa(); }
        //}
        //private void aa()
        //{
        //    switch (Position)
        //    {
        //        case PositionObject.Back:
        //            {
        //                _checkBox.SendToBack();
        //                break;
        //            }                    
        //        case PositionObject.Front:
        //            {
        //                _checkBox.BringToFront();
        //                break;
        //            }
        //        default:
        //            break;
        //    }
        //}
    }
}
