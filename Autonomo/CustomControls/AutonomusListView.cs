﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autonomo.CustomControls
{
    public class AutonomusListView: ListView
    {
        public AutonomusListView()
        {
            base.BorderStyle = BorderStyle.None;
            base.Font = new Font("Verdana", 10, FontStyle.Regular);
            base.GridLines = true;
            base.View = View.Details;
            base.Activation = ItemActivation.OneClick;
            base.HideSelection = false;
            base.HoverSelection = true;
            base.LabelWrap = false;
            base.Dock = DockStyle.None;
        }
    }
}
