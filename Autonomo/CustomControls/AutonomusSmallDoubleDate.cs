﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autonomo.CustomControls
{
    public partial class AutonomusSmallDoubleDate : UserControl
    {
        public Color lineMainColor;

        public AutonomusSmallDoubleDate()
        {
            InitializeComponent();
            dtDesde.Value = DateTime.Now;
            txDesde.Text = dtDesde.Value.ToShortDateString();
            dtHasta.Value = DateTime.Now;
            dtHasta.Text = dtHasta.Value.ToShortDateString();
        }

        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Bindable(true)]
        [Category("Autonomo properties")]
        public DateTime ValueFromDate
        {
            get { return dtDesde.Value; }
            set { dtDesde.Value = value; }
        }


        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Bindable(true)]
        [Category("Autonomo properties")]
        public DateTime ValueToDate
        {
            get { return dtHasta.Value; }
            set { dtHasta.Value = value; }
        }
        [Category("Autonomo properties")]
        public string Title
        {
            get { return title.Text; }
            set { title.Text = value; }
        }

        [Category("Autonomo properties")]
        public System.Windows.Forms.DateTimePickerFormat Format
        {
            get { return dtDesde.Format; }
            set { dtDesde.Format = value; dtHasta.Format = value; }
        }

        [Category("Autonomo properties")]
        public string CustomFromDateFormat
        {
            get { return dtDesde.CustomFormat; }
            set { dtDesde.CustomFormat = value; txDesde.Text = value; }
        }

        [Category("Autonomo properties")]
        public string CustomToDateFormat
        {
            get { return dtHasta.CustomFormat; }
            set { dtHasta.CustomFormat = value; txHasta.Text = value; }
        }

        [Category("Autonomo properties")]
        public Font FontFromText
        {
            get { return txDesde.Font; }
            set { txDesde.Font = value; }
        }

        [Category("Autonomo properties")]
        public Font FontToText
        {
            get { return txHasta.Font; }
            set { txHasta.Font = value; }
        }
        #region LabelBotColor - Config

        [Category("Autonomo properties")]
        public Color LineBotColor
        {
            get { return Linea.BackColor; }
            set { Linea.BackColor = value; }
        }

        [Category("Autonomo properties")]
        public int LineSize
        {
            get { return Linea.Height; }
            set { Linea.Height = value; }
        }

        [Category("Autonomo properties")]
        public Color ColorText
        {
            get { return dtDesde.ForeColor; }
            set { dtDesde.ForeColor = value; dtHasta.ForeColor = value; }
        }
        #endregion End LabelBotColor - Config

        private void lbCaption_Click(object sender, EventArgs e)
        {
            dtDesde.Select();
            SendKeys.Send("%{DOWN}");
        }

        private void cmdDown1_Click(object sender, EventArgs e)
        {
            dtDesde.Select();
            SendKeys.Send("%{DOWN}");
        }
        private void cmdDown2_Click(object sender, EventArgs e)
        {
            dtHasta.Select();
            SendKeys.Send("%{DOWN}");
        }

        [Category("Autonomo properties")]

        public event EventHandler DateChange;
        private void dtDate_onValueChanged(object sender, EventArgs e)
        {
            if (DateChange != null) DateChange(sender, e);
        }

        private void dtHasta_ValueChanged(object sender, EventArgs e)
        {
            txHasta.Text = dtHasta.Value.ToShortDateString();
            dtDate_onValueChanged(sender, e);
        }

        private void dtDesde_ValueChanged(object sender, EventArgs e)
        {
            txDesde.Text = dtDesde.Value.ToShortDateString();
            dtDate_onValueChanged(sender, e);
        }

        #region Text
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Bindable(true)]
        [Category("Autonomo properties")]
        public string TextFromDate
        {
            get { return txDesde.Text; }
            set
            {
                value = value ?? string.Empty;
                if (txDesde.Text != value)
                    txDesde.Text = value;
                // this.Invalidate();
            }
        }
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Bindable(true)]
        [Category("Autonomo properties")]
        public string TextToDate
        {
            get { return txHasta.Text; }
            set
            {
                value = value ?? string.Empty;
                if (txHasta.Text != value)
                    txHasta.Text = value;
                // this.Invalidate();
            }
        }

        [Category("Autonomo properties")]
        [Browsable(true)]
        public event EventHandler TextBoxChanged;
        public virtual void OnTextChanged()
        {
            if (TextBoxChanged != null)
                this.TextBoxChanged(this, EventArgs.Empty);
        }

        private void txDesde_TextChanged(object sender, EventArgs e)
        {
            //fromValidate();
            this.OnTextChanged();
        }
        private void txHasta_TextChanged(object sender, EventArgs e)
        {
            // toValidate();
            this.OnTextChanged();
        }
       
        private void inicioFromText()
        {
            txDesde.Text = txDesde.Text.Replace(" ", "");
            txDesde.SelectionStart = 0;
            txDesde.SelectAll();
            txDesde.Focus();
        }
        private void inicioToText()
        {
            txHasta.Text = txHasta.Text.Replace(" ", "");
            txHasta.SelectionStart = 0;
            txHasta.SelectAll();
            txHasta.Focus();
        }
      
        private void txDesde_Click(object sender, EventArgs e)
        {
            inicioFromText();
        }
        private void txHasta_Click(object sender, EventArgs e)
        {
            inicioToText();
        }

        #region Key Down / Key Press
        [Category("Autonomo properties")]
        [Browsable(true)]
        public new event EventHandler<KeyEventArgs> KeyDown;
        protected override void OnKeyDown(KeyEventArgs e)
        {
            var handler = KeyDown;
            if (handler != null) handler(this, e);
        }
        private void txDesde_KeyDown(object sender, KeyEventArgs e)
        {
            this.OnKeyDown(e);
        }

        private void txHasta_KeyDown(object sender, KeyEventArgs e)
        {
            this.OnKeyDown(e);
        }

        [Category("Autonomo properties")]
        [Browsable(true)]
        public new event EventHandler<KeyPressEventArgs> KeyPress;
        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            var handler = KeyPress;
            if (handler != null) handler(this, e);
        }
        private void txDesde_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.OnKeyPress(e);
        }
        private void txHasta_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.OnKeyPress(e);
        }

        //[Browsable(true)]
        //public new event EventHandler<EventArgs> Enter;
        protected override void OnEnter(EventArgs e)
        {
            base.OnEnter(e);
        }
        private void txDesde_Enter(object sender, EventArgs e)
        {
            inicioFromText();
            this.OnEnter(e);
        }
        private void txHasta_Enter(object sender, EventArgs e)
        {
            inicioToText();
            this.OnEnter(e);
        }

        //[Browsable(true)]
        //public new event EventHandler<EventArgs> Leave;

        protected override void OnLeave(EventArgs e)
        {
            base.OnLeave(e);
        }
        private void txDesde_Leave(object sender, EventArgs e)
        {
           // Autonomus.Object.Helpers.ValidateFormat(txDesde, dtDesde, Format);
            this.OnLeave(e);
        }
        private void txHasta_Leave(object sender, EventArgs e)
        {
            //Autonomus.Object.Helpers.ValidateFormat(txHasta, dtHasta, Format);
            this.OnLeave(e);
        }


        #endregion   
        #endregion

        private void FlatDoubleDate_Resize(object sender, EventArgs e)
        {
            int width = (pnlBack.Width / 2) - 4;
            pnLeft.Width = width;
            if (pnLeft.Width > 122)
            {
                imgDesde.Visible = true;
                imgHasta.Visible = true;
            }
            else
            {
                imgDesde.Visible = false;
                imgHasta.Visible = false;
            }
        }

        private void AutonomusSmallDoubleDate_Load(object sender, EventArgs e)
        {
            lineMainColor = Linea.BackColor;
        }
    }
}