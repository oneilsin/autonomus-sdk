﻿
namespace Autonomo.CustomControls
{
    partial class AutonomusEventsPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Contenedor = new System.Windows.Forms.Panel();
            this.LabelContainers = new System.Windows.Forms.FlowLayoutPanel();
            this.titlePanel = new System.Windows.Forms.Panel();
            this.Titulo = new System.Windows.Forms.Label();
            this.Contenedor.SuspendLayout();
            this.titlePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // Contenedor
            // 
            this.Contenedor.Controls.Add(this.LabelContainers);
            this.Contenedor.Controls.Add(this.titlePanel);
            this.Contenedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Contenedor.Location = new System.Drawing.Point(0, 0);
            this.Contenedor.Name = "Contenedor";
            this.Contenedor.Size = new System.Drawing.Size(278, 73);
            this.Contenedor.TabIndex = 0;
            // 
            // LabelContainers
            // 
            this.LabelContainers.AutoScroll = true;
            this.LabelContainers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LabelContainers.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.LabelContainers.Location = new System.Drawing.Point(0, 31);
            this.LabelContainers.Name = "LabelContainers";
            this.LabelContainers.Size = new System.Drawing.Size(278, 42);
            this.LabelContainers.TabIndex = 1;
            this.LabelContainers.WrapContents = false;
            // 
            // titlePanel
            // 
            this.titlePanel.Controls.Add(this.Titulo);
            this.titlePanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.titlePanel.Location = new System.Drawing.Point(0, 0);
            this.titlePanel.Name = "titlePanel";
            this.titlePanel.Size = new System.Drawing.Size(278, 31);
            this.titlePanel.TabIndex = 0;
            // 
            // Titulo
            // 
            this.Titulo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Titulo.Font = new System.Drawing.Font("Verdana", 10.25F, System.Drawing.FontStyle.Bold);
            this.Titulo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Titulo.Location = new System.Drawing.Point(0, 0);
            this.Titulo.Name = "Titulo";
            this.Titulo.Size = new System.Drawing.Size(278, 31);
            this.Titulo.TabIndex = 0;
            this.Titulo.Text = "--Title--";
            this.Titulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AutonomusEventsPanel
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.Contenedor);
            this.Name = "AutonomusEventsPanel";
            this.Size = new System.Drawing.Size(278, 73);
            this.Contenedor.ResumeLayout(false);
            this.titlePanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Contenedor;
        private System.Windows.Forms.Panel titlePanel;
        private System.Windows.Forms.Label Titulo;
        private System.Windows.Forms.FlowLayoutPanel LabelContainers;
    }
}
