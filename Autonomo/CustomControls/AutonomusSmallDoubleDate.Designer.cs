﻿namespace Autonomo.CustomControls
{
    partial class AutonomusSmallDoubleDate
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AutonomusSmallDoubleDate));
            this.pnlBack = new System.Windows.Forms.Panel();
            this.pnRight = new System.Windows.Forms.Panel();
            this.imgHasta = new System.Windows.Forms.Label();
            this.txHasta = new System.Windows.Forms.MaskedTextBox();
            this.lbTop2 = new System.Windows.Forms.Label();
            this.lbLeft2 = new System.Windows.Forms.Label();
            this.cmdDown2 = new System.Windows.Forms.Label();
            this.lb2 = new System.Windows.Forms.Label();
            this.dtHasta = new System.Windows.Forms.DateTimePicker();
            this.lbMidle = new System.Windows.Forms.Label();
            this.pnLeft = new System.Windows.Forms.Panel();
            this.imgDesde = new System.Windows.Forms.Label();
            this.txDesde = new System.Windows.Forms.MaskedTextBox();
            this.lbTop1 = new System.Windows.Forms.Label();
            this.lbLeft1 = new System.Windows.Forms.Label();
            this.cmdDown1 = new System.Windows.Forms.Label();
            this.lb1 = new System.Windows.Forms.Label();
            this.dtDesde = new System.Windows.Forms.DateTimePicker();
            this.lbSBot = new System.Windows.Forms.Label();
            this.Linea = new System.Windows.Forms.Label();
            this.title = new System.Windows.Forms.Label();
            this.pnlBack.SuspendLayout();
            this.pnRight.SuspendLayout();
            this.pnLeft.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlBack
            // 
            this.pnlBack.Controls.Add(this.pnRight);
            this.pnlBack.Controls.Add(this.lbMidle);
            this.pnlBack.Controls.Add(this.pnLeft);
            this.pnlBack.Controls.Add(this.lbSBot);
            this.pnlBack.Controls.Add(this.Linea);
            this.pnlBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBack.Location = new System.Drawing.Point(84, 0);
            this.pnlBack.Name = "pnlBack";
            this.pnlBack.Size = new System.Drawing.Size(256, 30);
            this.pnlBack.TabIndex = 1;
            // 
            // pnRight
            // 
            this.pnRight.Controls.Add(this.imgHasta);
            this.pnRight.Controls.Add(this.txHasta);
            this.pnRight.Controls.Add(this.lbTop2);
            this.pnRight.Controls.Add(this.lbLeft2);
            this.pnRight.Controls.Add(this.cmdDown2);
            this.pnRight.Controls.Add(this.lb2);
            this.pnRight.Controls.Add(this.dtHasta);
            this.pnRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnRight.Location = new System.Drawing.Point(130, 0);
            this.pnRight.Name = "pnRight";
            this.pnRight.Size = new System.Drawing.Size(126, 27);
            this.pnRight.TabIndex = 1;
            // 
            // imgHasta
            // 
            this.imgHasta.Dock = System.Windows.Forms.DockStyle.Right;
            this.imgHasta.Image = ((System.Drawing.Image)(resources.GetObject("imgHasta.Image")));
            this.imgHasta.Location = new System.Drawing.Point(86, 8);
            this.imgHasta.Name = "imgHasta";
            this.imgHasta.Size = new System.Drawing.Size(18, 19);
            this.imgHasta.TabIndex = 29;
            this.imgHasta.Visible = false;
            // 
            // txHasta
            // 
            this.txHasta.BackColor = System.Drawing.Color.White;
            this.txHasta.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txHasta.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txHasta.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txHasta.Location = new System.Drawing.Point(3, 8);
            this.txHasta.Mask = "00-00-0000";
            this.txHasta.Name = "txHasta";
            this.txHasta.PromptChar = ' ';
            this.txHasta.Size = new System.Drawing.Size(101, 15);
            this.txHasta.TabIndex = 0;
            this.txHasta.ValidatingType = typeof(System.DateTime);
            // 
            // lbTop2
            // 
            this.lbTop2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.lbTop2.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTop2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTop2.ForeColor = System.Drawing.Color.DimGray;
            this.lbTop2.Location = new System.Drawing.Point(3, 0);
            this.lbTop2.Name = "lbTop2";
            this.lbTop2.Size = new System.Drawing.Size(101, 8);
            this.lbTop2.TabIndex = 6;
            // 
            // lbLeft2
            // 
            this.lbLeft2.BackColor = System.Drawing.Color.White;
            this.lbLeft2.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbLeft2.Location = new System.Drawing.Point(0, 0);
            this.lbLeft2.Name = "lbLeft2";
            this.lbLeft2.Size = new System.Drawing.Size(3, 27);
            this.lbLeft2.TabIndex = 5;
            // 
            // cmdDown2
            // 
            this.cmdDown2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdDown2.Dock = System.Windows.Forms.DockStyle.Right;
            this.cmdDown2.Image = ((System.Drawing.Image)(resources.GetObject("cmdDown2.Image")));
            this.cmdDown2.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdDown2.Location = new System.Drawing.Point(104, 0);
            this.cmdDown2.Name = "cmdDown2";
            this.cmdDown2.Size = new System.Drawing.Size(22, 27);
            this.cmdDown2.TabIndex = 25;
            // 
            // lb2
            // 
            this.lb2.Location = new System.Drawing.Point(0, -9);
            this.lb2.Name = "lb2";
            this.lb2.Size = new System.Drawing.Size(147, 59);
            this.lb2.TabIndex = 26;
            // 
            // dtHasta
            // 
            this.dtHasta.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtHasta.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtHasta.Location = new System.Drawing.Point(3, 20);
            this.dtHasta.Name = "dtHasta";
            this.dtHasta.Size = new System.Drawing.Size(120, 22);
            this.dtHasta.TabIndex = 23;
            // 
            // lbMidle
            // 
            this.lbMidle.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbMidle.Location = new System.Drawing.Point(122, 0);
            this.lbMidle.Name = "lbMidle";
            this.lbMidle.Size = new System.Drawing.Size(8, 27);
            this.lbMidle.TabIndex = 28;
            // 
            // pnLeft
            // 
            this.pnLeft.Controls.Add(this.imgDesde);
            this.pnLeft.Controls.Add(this.txDesde);
            this.pnLeft.Controls.Add(this.lbTop1);
            this.pnLeft.Controls.Add(this.lbLeft1);
            this.pnLeft.Controls.Add(this.cmdDown1);
            this.pnLeft.Controls.Add(this.lb1);
            this.pnLeft.Controls.Add(this.dtDesde);
            this.pnLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnLeft.Location = new System.Drawing.Point(0, 0);
            this.pnLeft.Name = "pnLeft";
            this.pnLeft.Size = new System.Drawing.Size(122, 27);
            this.pnLeft.TabIndex = 0;
            // 
            // imgDesde
            // 
            this.imgDesde.Dock = System.Windows.Forms.DockStyle.Right;
            this.imgDesde.Image = ((System.Drawing.Image)(resources.GetObject("imgDesde.Image")));
            this.imgDesde.Location = new System.Drawing.Point(82, 8);
            this.imgDesde.Name = "imgDesde";
            this.imgDesde.Size = new System.Drawing.Size(18, 19);
            this.imgDesde.TabIndex = 28;
            this.imgDesde.Visible = false;
            // 
            // txDesde
            // 
            this.txDesde.BackColor = System.Drawing.Color.White;
            this.txDesde.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txDesde.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txDesde.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txDesde.Location = new System.Drawing.Point(3, 8);
            this.txDesde.Mask = "00-00-0000";
            this.txDesde.Name = "txDesde";
            this.txDesde.PromptChar = ' ';
            this.txDesde.Size = new System.Drawing.Size(97, 15);
            this.txDesde.TabIndex = 0;
            this.txDesde.Text = "20202020";
            this.txDesde.ValidatingType = typeof(System.DateTime);
            // 
            // lbTop1
            // 
            this.lbTop1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.lbTop1.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTop1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTop1.ForeColor = System.Drawing.Color.DimGray;
            this.lbTop1.Location = new System.Drawing.Point(3, 0);
            this.lbTop1.Name = "lbTop1";
            this.lbTop1.Size = new System.Drawing.Size(97, 8);
            this.lbTop1.TabIndex = 6;
            // 
            // lbLeft1
            // 
            this.lbLeft1.BackColor = System.Drawing.Color.White;
            this.lbLeft1.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbLeft1.Location = new System.Drawing.Point(0, 0);
            this.lbLeft1.Name = "lbLeft1";
            this.lbLeft1.Size = new System.Drawing.Size(3, 27);
            this.lbLeft1.TabIndex = 5;
            // 
            // cmdDown1
            // 
            this.cmdDown1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdDown1.Dock = System.Windows.Forms.DockStyle.Right;
            this.cmdDown1.Image = ((System.Drawing.Image)(resources.GetObject("cmdDown1.Image")));
            this.cmdDown1.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdDown1.Location = new System.Drawing.Point(100, 0);
            this.cmdDown1.Name = "cmdDown1";
            this.cmdDown1.Size = new System.Drawing.Size(22, 27);
            this.cmdDown1.TabIndex = 25;
            // 
            // lb1
            // 
            this.lb1.Location = new System.Drawing.Point(0, -9);
            this.lb1.Name = "lb1";
            this.lb1.Size = new System.Drawing.Size(137, 59);
            this.lb1.TabIndex = 26;
            // 
            // dtDesde
            // 
            this.dtDesde.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtDesde.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDesde.Location = new System.Drawing.Point(3, 20);
            this.dtDesde.Name = "dtDesde";
            this.dtDesde.Size = new System.Drawing.Size(116, 22);
            this.dtDesde.TabIndex = 23;
            // 
            // lbSBot
            // 
            this.lbSBot.BackColor = System.Drawing.Color.White;
            this.lbSBot.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lbSBot.Location = new System.Drawing.Point(0, 27);
            this.lbSBot.Name = "lbSBot";
            this.lbSBot.Size = new System.Drawing.Size(256, 2);
            this.lbSBot.TabIndex = 25;
            // 
            // Linea
            // 
            this.Linea.BackColor = System.Drawing.Color.White;
            this.Linea.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Linea.Location = new System.Drawing.Point(0, 29);
            this.Linea.Name = "Linea";
            this.Linea.Size = new System.Drawing.Size(256, 1);
            this.Linea.TabIndex = 3;
            // 
            // title
            // 
            this.title.Dock = System.Windows.Forms.DockStyle.Left;
            this.title.Font = new System.Drawing.Font("Verdana", 9F);
            this.title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.title.Location = new System.Drawing.Point(0, 0);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(84, 30);
            this.title.TabIndex = 25;
            this.title.Text = "Date range:";
            this.title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AutonomusSmallDoubleDate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.pnlBack);
            this.Controls.Add(this.title);
            this.Name = "AutonomusSmallDoubleDate";
            this.Size = new System.Drawing.Size(340, 30);
            this.Load += new System.EventHandler(this.AutonomusSmallDoubleDate_Load);
            this.pnlBack.ResumeLayout(false);
            this.pnRight.ResumeLayout(false);
            this.pnRight.PerformLayout();
            this.pnLeft.ResumeLayout(false);
            this.pnLeft.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlBack;
        private System.Windows.Forms.Panel pnRight;
        private System.Windows.Forms.Label imgHasta;
        private System.Windows.Forms.Label lbTop2;
        private System.Windows.Forms.Label lbLeft2;
        private System.Windows.Forms.Label lb2;
        public System.Windows.Forms.DateTimePicker dtHasta;
        private System.Windows.Forms.Label lbMidle;
        private System.Windows.Forms.Panel pnLeft;
        private System.Windows.Forms.Label imgDesde;
        private System.Windows.Forms.Label lbLeft1;
        private System.Windows.Forms.Label lb1;
        public System.Windows.Forms.DateTimePicker dtDesde;
        private System.Windows.Forms.Label lbSBot;
        private System.Windows.Forms.Label Linea;
        private System.Windows.Forms.Label title;
        private System.Windows.Forms.Label lbTop1;
        protected System.Windows.Forms.Label cmdDown2;
        private System.Windows.Forms.Label cmdDown1;
        private System.Windows.Forms.MaskedTextBox txHasta;
        private System.Windows.Forms.MaskedTextBox txDesde;
    }
}
