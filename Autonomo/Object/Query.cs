﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Autonomo.CustomControls.HelperControl;

namespace Autonomo.Object
{
    public partial class Query : Form
    {
        /// <summary>
        /// Este enumerable nos sirve para saber el estado del Formulario.
        /// </summary>
        State StateForm;
        public Query()
        {
            InitializeComponent();
            StateForm = State.Cancel;
        }

        public void ThemeStyle(Theme theme)
        {
            switch (theme)
            {
                case Theme.White:
                    break;
                case Theme.Dark:
                    break;
                case Theme.BlueDark:
                    break;
                case Theme.Green:
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Atributo que cambia el color de fondo de la cabecera de la grilla.
        /// </summary>
        [Category("Custom Autonomo")]
        public Color GridHeadBackColor
        {
            get { return grdData.HeaderColor; }
            set { grdData.HeaderColor = value; }
        }

        /// <summary>
        /// Atributo que cambia el color de la letra en la cabecera de la grilla.
        /// </summary>
        [Category("Custom Autonomo")]
        public Color GridHeadForeColor
        {
            get { return grdData.HeaderForeColor; }
            set { grdData.HeaderForeColor = value; }
        }

        /// <summary>
        /// Evento para seleccionar la data de la grilla; esto, se ejecuta al hacer doble click sobre la grilla.
        /// </summary>
        [Browsable(true)]
        [Category("Custom Autonomo")]
        public event DataGridViewCellEventHandler CellContentDoubleClick;

        /// <summary>
        /// Evento para seleccionar la data de la grilla; esto, se ejecuta al presionar la tecla Enter sobre la grilla.
        /// </summary>
        [Browsable(true)]
        [Category("Custom Autonomo")]
        public event EventHandler<KeyEventArgs> CellContentKeyDown;

        /// <summary>
        /// Metodo para configurar el formulario; aplicar borde redondeado.
        /// </summary>
        public void ConfigFormulary()
        {
            Autonomo.Class.RoundObject.RoundForm(this, 20, 20);
        }

        /// <summary>
        /// Metodo para Seleccionar la data y cerrar el formulario
        /// </summary>
        public void Set()
        {
            //this.Tag = "Get";
            StateForm = State.Success;
            this.Close();
        }

        /// <summary>
        /// Metodo para Cancelar y cerrar el formulario
        /// </summary>
        private void Cancel()
        {
            // this.Tag = "None";
            StateForm = State.Cancel;
            this.Close();
        }

        /// <summary>
        /// Estado del formulario, nos permite validar si se ha seleccionado los valores de la grilla 
        /// o si se ha cerrado el formulario sin seleccionar la data.
        /// </summary>
        public bool StateFormulary
        {
            get
            {
                return StateForm == State.Success ? true : false;
            }
        }

        /// <summary>
        /// Mover el formulario manteniendo Click sobre el titulo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Title_MouseDown(object sender, MouseEventArgs e)
        {
            Class.MoveObject.MoveForm(this);
        }

        /// <summary>
        /// Cerrar el formulario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdClose_Click(object sender, EventArgs e)
        {
            Cancel();
        }

        /// <summary>
        /// Para activar el evento CellContentDoubleClick y cambiar el estado del formulario.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (grdData.Rows.Count > 0)
            {
                if (CellContentDoubleClick != null)
                    CellContentDoubleClick(sender, e);

                this.Set();
            }
        }

        /// <summary>
        /// Para que el cursor se quede en la fila seleccionada, dentro de la grilla.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Query_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r' || e.KeyChar == (char)Keys.Escape)
                e.Handled = true;
        }

        /// <summary>
        /// Para Cerrar el formulario presionando la tecla Scape
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Query_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape) { Cancel(); }
        }

        /// <summary>
        /// Para Activar el evento CellContentKeyDown y Cambiar el estado del Formulario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdData_KeyDown(object sender, KeyEventArgs e)
        {
            if (grdData.Rows.Count > 0)
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (CellContentKeyDown != null)
                        CellContentKeyDown(sender, e);

                    this.Set();
                }
            }
        }

        /// <summary>
        /// Para entrar a la grilla utilizando la tecla hacia abajo.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txConsulta_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                grdData.Focus();
            }
        }
    }
}
