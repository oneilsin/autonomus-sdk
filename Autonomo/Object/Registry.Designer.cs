﻿namespace Autonomo.Object
{
    partial class Registry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Registry));
            this.pnlContenedor = new System.Windows.Forms.Panel();
            this.pnlGrid = new System.Windows.Forms.Panel();
            this.pnlBottonControl = new System.Windows.Forms.Panel();
            this.DescriptionData = new System.Windows.Forms.Label();
            this.BottomLine = new System.Windows.Forms.Label();
            this.pnlTopControl = new System.Windows.Forms.Panel();
            this.pnlTopButton = new System.Windows.Forms.Panel();
            this.btnCommand3 = new System.Windows.Forms.Button();
            this.btnCommand2 = new System.Windows.Forms.Button();
            this.btnCommand1 = new System.Windows.Forms.Button();
            this.btnExportar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnEditar = new System.Windows.Forms.Button();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Title = new System.Windows.Forms.Label();
            this.TopLine = new System.Windows.Forms.Label();
            this.pnlContenedor.SuspendLayout();
            this.pnlBottonControl.SuspendLayout();
            this.pnlTopControl.SuspendLayout();
            this.pnlTopButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlContenedor
            // 
            this.pnlContenedor.Controls.Add(this.pnlGrid);
            this.pnlContenedor.Controls.Add(this.pnlBottonControl);
            this.pnlContenedor.Controls.Add(this.pnlTopControl);
            this.pnlContenedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlContenedor.Location = new System.Drawing.Point(0, 0);
            this.pnlContenedor.Name = "pnlContenedor";
            this.pnlContenedor.Size = new System.Drawing.Size(800, 320);
            this.pnlContenedor.TabIndex = 0;
            // 
            // pnlGrid
            // 
            this.pnlGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlGrid.Location = new System.Drawing.Point(0, 112);
            this.pnlGrid.Name = "pnlGrid";
            this.pnlGrid.Size = new System.Drawing.Size(800, 176);
            this.pnlGrid.TabIndex = 1;
            // 
            // pnlBottonControl
            // 
            this.pnlBottonControl.Controls.Add(this.DescriptionData);
            this.pnlBottonControl.Controls.Add(this.BottomLine);
            this.pnlBottonControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottonControl.Location = new System.Drawing.Point(0, 288);
            this.pnlBottonControl.Name = "pnlBottonControl";
            this.pnlBottonControl.Size = new System.Drawing.Size(800, 32);
            this.pnlBottonControl.TabIndex = 1;
            // 
            // DescriptionData
            // 
            this.DescriptionData.AutoSize = true;
            this.DescriptionData.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DescriptionData.ForeColor = System.Drawing.Color.DimGray;
            this.DescriptionData.Location = new System.Drawing.Point(12, 3);
            this.DescriptionData.Name = "DescriptionData";
            this.DescriptionData.Size = new System.Drawing.Size(94, 16);
            this.DescriptionData.TabIndex = 3;
            this.DescriptionData.Text = "Some data...";
            // 
            // BottomLine
            // 
            this.BottomLine.BackColor = System.Drawing.SystemColors.Highlight;
            this.BottomLine.Dock = System.Windows.Forms.DockStyle.Top;
            this.BottomLine.Location = new System.Drawing.Point(0, 0);
            this.BottomLine.Name = "BottomLine";
            this.BottomLine.Size = new System.Drawing.Size(800, 2);
            this.BottomLine.TabIndex = 3;
            // 
            // pnlTopControl
            // 
            this.pnlTopControl.Controls.Add(this.pnlTopButton);
            this.pnlTopControl.Controls.Add(this.Title);
            this.pnlTopControl.Controls.Add(this.TopLine);
            this.pnlTopControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTopControl.Location = new System.Drawing.Point(0, 0);
            this.pnlTopControl.Name = "pnlTopControl";
            this.pnlTopControl.Size = new System.Drawing.Size(800, 112);
            this.pnlTopControl.TabIndex = 0;
            // 
            // pnlTopButton
            // 
            this.pnlTopButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            this.pnlTopButton.Controls.Add(this.btnCommand3);
            this.pnlTopButton.Controls.Add(this.btnCommand2);
            this.pnlTopButton.Controls.Add(this.btnCommand1);
            this.pnlTopButton.Controls.Add(this.btnExportar);
            this.pnlTopButton.Controls.Add(this.btnEliminar);
            this.pnlTopButton.Controls.Add(this.btnEditar);
            this.pnlTopButton.Controls.Add(this.btnNuevo);
            this.pnlTopButton.Controls.Add(this.label1);
            this.pnlTopButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlTopButton.Location = new System.Drawing.Point(0, 82);
            this.pnlTopButton.Name = "pnlTopButton";
            this.pnlTopButton.Size = new System.Drawing.Size(800, 28);
            this.pnlTopButton.TabIndex = 1;
            // 
            // btnCommand3
            // 
            this.btnCommand3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCommand3.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnCommand3.FlatAppearance.BorderSize = 0;
            this.btnCommand3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCommand3.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCommand3.Location = new System.Drawing.Point(588, 0);
            this.btnCommand3.Name = "btnCommand3";
            this.btnCommand3.Size = new System.Drawing.Size(104, 28);
            this.btnCommand3.TabIndex = 7;
            this.btnCommand3.Text = "Command_3";
            this.btnCommand3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCommand3.UseVisualStyleBackColor = true;
            this.btnCommand3.Visible = false;
            // 
            // btnCommand2
            // 
            this.btnCommand2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCommand2.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnCommand2.FlatAppearance.BorderSize = 0;
            this.btnCommand2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCommand2.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCommand2.Location = new System.Drawing.Point(484, 0);
            this.btnCommand2.Name = "btnCommand2";
            this.btnCommand2.Size = new System.Drawing.Size(104, 28);
            this.btnCommand2.TabIndex = 6;
            this.btnCommand2.Text = "Command_2";
            this.btnCommand2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCommand2.UseVisualStyleBackColor = true;
            this.btnCommand2.Visible = false;
            // 
            // btnCommand1
            // 
            this.btnCommand1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCommand1.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnCommand1.FlatAppearance.BorderSize = 0;
            this.btnCommand1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCommand1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCommand1.Location = new System.Drawing.Point(380, 0);
            this.btnCommand1.Name = "btnCommand1";
            this.btnCommand1.Size = new System.Drawing.Size(104, 28);
            this.btnCommand1.TabIndex = 5;
            this.btnCommand1.Text = "Command_1";
            this.btnCommand1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCommand1.UseVisualStyleBackColor = true;
            this.btnCommand1.Visible = false;
            // 
            // btnExportar
            // 
            this.btnExportar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExportar.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnExportar.FlatAppearance.BorderSize = 0;
            this.btnExportar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExportar.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportar.Image = ((System.Drawing.Image)(resources.GetObject("btnExportar.Image")));
            this.btnExportar.Location = new System.Drawing.Point(276, 0);
            this.btnExportar.Name = "btnExportar";
            this.btnExportar.Size = new System.Drawing.Size(104, 28);
            this.btnExportar.TabIndex = 3;
            this.btnExportar.Text = "Exportar";
            this.btnExportar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExportar.UseVisualStyleBackColor = true;
            // 
            // btnEliminar
            // 
            this.btnEliminar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEliminar.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnEliminar.FlatAppearance.BorderSize = 0;
            this.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminar.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminar.Image = ((System.Drawing.Image)(resources.GetObject("btnEliminar.Image")));
            this.btnEliminar.Location = new System.Drawing.Point(183, 0);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(93, 28);
            this.btnEliminar.TabIndex = 2;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEliminar.UseVisualStyleBackColor = true;
            // 
            // btnEditar
            // 
            this.btnEditar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEditar.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnEditar.FlatAppearance.BorderSize = 0;
            this.btnEditar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditar.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditar.Image = ((System.Drawing.Image)(resources.GetObject("btnEditar.Image")));
            this.btnEditar.Location = new System.Drawing.Point(100, 0);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(83, 28);
            this.btnEditar.TabIndex = 1;
            this.btnEditar.Text = "Editar";
            this.btnEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEditar.UseVisualStyleBackColor = true;
            // 
            // btnNuevo
            // 
            this.btnNuevo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNuevo.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnNuevo.FlatAppearance.BorderSize = 0;
            this.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNuevo.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNuevo.Image = ((System.Drawing.Image)(resources.GetObject("btnNuevo.Image")));
            this.btnNuevo.Location = new System.Drawing.Point(17, 0);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(83, 28);
            this.btnNuevo.TabIndex = 0;
            this.btnNuevo.Text = "Nuevo";
            this.btnNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNuevo.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 28);
            this.label1.TabIndex = 4;
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.Location = new System.Drawing.Point(12, 9);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(153, 18);
            this.Title.TabIndex = 0;
            this.Title.Text = "Formulary Name";
            // 
            // TopLine
            // 
            this.TopLine.BackColor = System.Drawing.SystemColors.Highlight;
            this.TopLine.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TopLine.Location = new System.Drawing.Point(0, 110);
            this.TopLine.Name = "TopLine";
            this.TopLine.Size = new System.Drawing.Size(800, 2);
            this.TopLine.TabIndex = 2;
            // 
            // Registry
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(800, 320);
            this.Controls.Add(this.pnlContenedor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "Registry";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Registry";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Registry_KeyPress);
            this.pnlContenedor.ResumeLayout(false);
            this.pnlBottonControl.ResumeLayout(false);
            this.pnlBottonControl.PerformLayout();
            this.pnlTopControl.ResumeLayout(false);
            this.pnlTopControl.PerformLayout();
            this.pnlTopButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel pnlContenedor;
        public System.Windows.Forms.Panel pnlGrid;
        public System.Windows.Forms.Panel pnlBottonControl;
        public System.Windows.Forms.Panel pnlTopControl;
        public System.Windows.Forms.Panel pnlTopButton;
        public System.Windows.Forms.Label BottomLine;
        public System.Windows.Forms.Label TopLine;
        public System.Windows.Forms.Label DescriptionData;
        public System.Windows.Forms.Label Title;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button btnExportar;
        public System.Windows.Forms.Button btnEliminar;
        public System.Windows.Forms.Button btnEditar;
        public System.Windows.Forms.Button btnNuevo;
        public System.Windows.Forms.Button btnCommand3;
        public System.Windows.Forms.Button btnCommand2;
        public System.Windows.Forms.Button btnCommand1;
    }
}