﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Autonomo.CustomControls.HelperControl;

namespace Autonomo.Object
{
    public partial class Modal : Form
    {
        /// <summary>
        /// Este enumerable nos sirve para saber el estado del Formulario.
        /// </summary>
        
        State StateForm;
        public Modal()
        {
            InitializeComponent();
            StateForm = State.Cancel;
        }

   
        public void ThemeStyle( Theme theme)
        {
            switch (theme)
            {
                case Theme.White:
                    break;
                case Theme.Dark:
                    break;
                case Theme.BlueDark:
                    break;
                case Theme.Green:
                    break;
                default:
                    break;
            }
        }
        public void ConfigFormulary()
        {
            Autonomo.Class.RoundObject.RoundForm(this, 20, 20);           
        }
        public void ConfigButton(Color buttonBackColor, Color buttonForeColor,int x, int y)
        {
            btnSave.BackColor = buttonBackColor;
            btnSave.ForeColor = buttonForeColor;
            Class.RoundObject.RoundButton(btnSave, x, y);
        }
        public void Set()
        {
            this.Tag = "Get";
            StateForm = State.Success;
            this.Close();
        }

        private void Cancel()
        {
            this.Tag = "None";
            StateForm = State.Cancel;
            this.Close();
        }

        /// <summary>
        /// Estado del formulario, nos permite validar si se ha seleccionado los valores de la grilla 
        /// o si se ha cerrado el formulario sin seleccionar la data.
        /// </summary>
        public bool StateFormulary
        {
            get
            {
                return StateForm == State.Success ? true : false;
            }
        }

        private void cmdClose_Click(object sender, EventArgs e)
        {
            Cancel();
        }

        private void Title_MouseDown(object sender, MouseEventArgs e)
        {
            Class.MoveObject.MoveForm(this);
        }

        private void Modal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r' || e.KeyChar == (char)Keys.Escape)
                e.Handled = true;
        }

        private void Modal_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape) { Cancel(); }
        }
    }
}
