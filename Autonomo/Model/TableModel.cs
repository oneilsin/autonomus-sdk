﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Autonomo.Class.Enumerable;

namespace Autonomo.Model
{
    public class TableModel
    {
        public string TableName { get; set; }
        public string SetColumn { get; set; }
        public string WhereColumn { get; set; }
        public string SetValue { get; set; }
        public string WhereValue { get; set; }
        public SqlIdentity Identity { get; set; }
    }
}